-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 29-Jan-2019 às 11:43
-- Versão do servidor: 5.6.41
-- versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amandaka_amandaschautica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_commentmeta`
--

CREATE TABLE `as_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_comments`
--

CREATE TABLE `as_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_comments`
--

INSERT INTO `as_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-01-16 11:14:07', '2019-01-16 13:14:07', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_links`
--

CREATE TABLE `as_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_options`
--

CREATE TABLE `as_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_options`
--

INSERT INTO `as_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://amandakaroline.com.br/amandaschautica', 'yes'),
(2, 'home', 'https://amandakaroline.com.br/amandaschautica', 'yes'),
(3, 'blogname', 'Amanda Schautica', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:109:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"linha-do-tempo/?$\";s:32:\"index.php?post_type=linhadotempo\";s:47:\"linha-do-tempo/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=linhadotempo&feed=$matches[1]\";s:42:\"linha-do-tempo/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=linhadotempo&feed=$matches[1]\";s:34:\"linha-do-tempo/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=linhadotempo&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:42:\"linha-do-tempo/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:52:\"linha-do-tempo/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:72:\"linha-do-tempo/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"linha-do-tempo/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"linha-do-tempo/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:48:\"linha-do-tempo/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"linha-do-tempo/([^/]+)/embed/?$\";s:45:\"index.php?linhadotempo=$matches[1]&embed=true\";s:35:\"linha-do-tempo/([^/]+)/trackback/?$\";s:39:\"index.php?linhadotempo=$matches[1]&tb=1\";s:55:\"linha-do-tempo/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?linhadotempo=$matches[1]&feed=$matches[2]\";s:50:\"linha-do-tempo/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?linhadotempo=$matches[1]&feed=$matches[2]\";s:43:\"linha-do-tempo/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?linhadotempo=$matches[1]&paged=$matches[2]\";s:50:\"linha-do-tempo/([^/]+)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?linhadotempo=$matches[1]&cpage=$matches[2]\";s:39:\"linha-do-tempo/([^/]+)(?:/([0-9]+))?/?$\";s:51:\"index.php?linhadotempo=$matches[1]&page=$matches[2]\";s:31:\"linha-do-tempo/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"linha-do-tempo/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"linha-do-tempo/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"linha-do-tempo/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"linha-do-tempo/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"linha-do-tempo/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(283, 'disqus_secret_key', 'qiKuTNoDXcrw7esVj5DLdEft0szCfYuUJWD0W0JCXb6xEXDRv9OvsvL3teVpUJ9q', 'yes'),
(284, 'disqus_admin_access_token', 'd339dc06ab5d4f92975587e5727f379e', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:44:\"base-amandaschautica/base-minhacasasolar.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:32:\"disqus-comment-system/disqus.php\";i:4;s:33:\"duplicate-post/duplicate-post.php\";i:5;s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";i:6;s:29:\"wp-postviews/wp-postviews.php\";i:7;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'amandaschautica', 'yes'),
(41, 'stylesheet', 'amandaschautica', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '43764', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'as_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:7:\"views-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(196, 'views_options', 'a:11:{s:5:\"count\";i:1;s:12:\"exclude_bots\";i:0;s:12:\"display_home\";i:0;s:14:\"display_single\";i:0;s:12:\"display_page\";i:0;s:15:\"display_archive\";i:0;s:14:\"display_search\";i:0;s:13:\"display_other\";i:0;s:8:\"use_ajax\";i:1;s:8:\"template\";s:18:\"%VIEW_COUNT% views\";s:20:\"most_viewed_template\";s:89:\"<li><a href=\"%POST_URL%\"  title=\"%POST_TITLE%\">%POST_TITLE%</a> - %VIEW_COUNT% views</li>\";}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1548769807;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1548771247;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1548810847;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1548854098;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1547645582;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(150, 'current_theme', 'amandaschautica', 'yes'),
(151, 'theme_mods_amandaschautica', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:15:\"amandaschautica\";i:11;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1547726503;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:7:\"views-2\";}}}}', 'yes'),
(152, 'theme_switched', '', 'yes'),
(153, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(451, '_site_transient_timeout_theme_roots', '1548770544', 'no'),
(452, '_site_transient_theme_roots', 'a:1:{s:15:\"amandaschautica\";s:7:\"/themes\";}', 'no'),
(453, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1548768744;s:7:\"checked\";a:14:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:7:\"4.3.8.3\";s:44:\"base-amandaschautica/base-minhacasasolar.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:32:\"disqus-comment-system/disqus.php\";s:6:\"3.0.16\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.2\";s:28:\"wysija-newsletters/index.php\";s:6:\"2.10.2\";s:21:\"mailpoet/mailpoet.php\";s:6:\"3.18.0\";s:21:\"meta-box/meta-box.php\";s:6:\"4.15.9\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:25:\"simple-tooltips/index.php\";s:5:\"2.1.3\";s:41:\"wordpress-importer/wordpress-importer.php\";s:5:\"0.6.4\";s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";s:5:\"4.2.2\";s:29:\"wp-postviews/wp-postviews.php\";s:6:\"1.76.1\";s:24:\"wordpress-seo/wp-seo.php\";s:3:\"9.4\";}s:8:\"response\";a:2:{s:21:\"mailpoet/mailpoet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/mailpoet\";s:4:\"slug\";s:8:\"mailpoet\";s:6:\"plugin\";s:21:\"mailpoet/mailpoet.php\";s:11:\"new_version\";s:6:\"3.18.2\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/mailpoet/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/mailpoet.3.18.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/mailpoet/assets/icon-256x256.png?rev=1895745\";s:2:\"1x\";s:61:\"https://ps.w.org/mailpoet/assets/icon-128x128.png?rev=1706492\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/mailpoet/assets/banner-1544x500.png?rev=2016863\";s:2:\"1x\";s:63:\"https://ps.w.org/mailpoet/assets/banner-772x250.png?rev=2016863\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"9.5\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wordpress-seo.9.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.0.3\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:11:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:7:\"4.3.8.3\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:6:\"3.0.16\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.16.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:2:\"1x\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:6:\"2.10.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.10.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:2:\"1x\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.15.9\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.15.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"simple-tooltips/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/simple-tooltips\";s:4:\"slug\";s:15:\"simple-tooltips\";s:6:\"plugin\";s:25:\"simple-tooltips/index.php\";s:11:\"new_version\";s:5:\"2.1.3\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/simple-tooltips/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/simple-tooltips.2.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/simple-tooltips/assets/icon-256x256.png?rev=1032140\";s:2:\"1x\";s:68:\"https://ps.w.org/simple-tooltips/assets/icon-128x128.png?rev=1032140\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/simple-tooltips/assets/banner-772x250.png?rev=808472\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:5:\"0.6.4\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wordpress-importer/assets/icon-256x256.png?rev=1908375\";s:2:\"1x\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";s:3:\"svg\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/wordpress-popular-posts\";s:4:\"slug\";s:23:\"wordpress-popular-posts\";s:6:\"plugin\";s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";s:11:\"new_version\";s:5:\"4.2.2\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/wordpress-popular-posts/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/wordpress-popular-posts.4.2.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/wordpress-popular-posts/assets/icon-256x256.png?rev=1232659\";s:2:\"1x\";s:76:\"https://ps.w.org/wordpress-popular-posts/assets/icon-128x128.png?rev=1232659\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/wordpress-popular-posts/assets/banner-772x250.png?rev=1130670\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-postviews/wp-postviews.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-postviews\";s:4:\"slug\";s:12:\"wp-postviews\";s:6:\"plugin\";s:29:\"wp-postviews/wp-postviews.php\";s:11:\"new_version\";s:6:\"1.76.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-postviews/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wp-postviews.1.76.1.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:56:\"https://ps.w.org/wp-postviews/assets/icon.svg?rev=978002\";s:3:\"svg\";s:56:\"https://ps.w.org/wp-postviews/assets/icon.svg?rev=978002\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-postviews/assets/banner-1544x500.jpg?rev=1206762\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-postviews/assets/banner-772x250.jpg?rev=1206762\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(154, 'configuracao', 'a:14:{s:8:\"last_tab\";s:0:\"\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:85:\"https://amandakaroline.com.br/amandaschautica/wp-content/uploads/2019/01/LOGO-PNG.png\";s:2:\"id\";s:2:\"64\";s:6:\"height\";s:3:\"166\";s:5:\"width\";s:3:\"359\";s:9:\"thumbnail\";s:93:\"https://amandakaroline.com.br/amandaschautica/wp-content/uploads/2019/01/LOGO-PNG-150x150.png\";s:5:\"title\";s:8:\"LOGO PNG\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"rodape_titulo_texto\";s:9:\"Sobre mim\";s:12:\"rodape_texto\";s:216:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quam nulla porttitor massa id neque aliquam vestibulum morbi. Morbi tristique senectus \";s:16:\"bannerPropaganda\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:11:\"link_banner\";s:0:\"\";s:12:\"opt_facebook\";s:1:\"#\";s:13:\"opt_instagram\";s:1:\"#\";s:12:\"opt_linkedin\";s:0:\"\";s:10:\"opt_google\";s:0:\"\";s:11:\"opt_youtube\";s:1:\"#\";s:12:\"opt_endereco\";s:48:\"Rua Saldanha Marinho, 1468, Guarapuava - Paraná\";s:12:\"opt_contatos\";a:2:{i:0;s:15:\"(42) 99999-9999\";i:1;s:15:\"(42) 98888-8888\";}s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(155, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1548435972;}', 'yes'),
(409, 'category_children', 'a:0:{}', 'yes'),
(126, 'can_compress_scripts', '1', 'no'),
(390, '_site_transient_timeout_browser_0ac1f9240df96b3586c220faef490724', '1549028187', 'no'),
(391, '_site_transient_browser_0ac1f9240df96b3586c220faef490724', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(224, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1547717101;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(237, 'duplicate_post_copystatus', '0', 'yes'),
(238, 'duplicate_post_copyslug', '0', 'yes'),
(236, 'duplicate_post_copydate', '0', 'yes'),
(229, 'theme_mods_minhacasasolar_blog', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1547726520;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:7:\"views-2\";}}}}', 'yes'),
(235, 'duplicate_post_copytitle', '1', 'yes'),
(407, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(252, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(253, 'duplicate_post_show_row', '1', 'yes'),
(254, 'duplicate_post_show_adminbar', '1', 'yes'),
(255, 'duplicate_post_show_submitbox', '1', 'yes'),
(256, 'duplicate_post_show_bulkactions', '1', 'yes'),
(257, 'duplicate_post_version', '3.2.2', 'yes'),
(258, 'duplicate_post_show_notice', '1', 'no'),
(141, 'recently_activated', 'a:0:{}', 'yes'),
(239, 'duplicate_post_copyexcerpt', '1', 'yes'),
(240, 'duplicate_post_copycontent', '1', 'yes'),
(241, 'duplicate_post_copythumbnail', '1', 'yes'),
(242, 'duplicate_post_copytemplate', '1', 'yes'),
(243, 'duplicate_post_copyformat', '1', 'yes'),
(244, 'duplicate_post_copyauthor', '0', 'yes'),
(245, 'duplicate_post_copypassword', '0', 'yes'),
(246, 'duplicate_post_copyattachments', '0', 'yes'),
(247, 'duplicate_post_copychildren', '0', 'yes'),
(248, 'duplicate_post_copycomments', '0', 'yes'),
(249, 'duplicate_post_copymenuorder', '1', 'yes'),
(250, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(251, 'duplicate_post_blacklist', '', 'yes'),
(185, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(186, 'wysija_post_type_updated', '1547655122', 'yes'),
(187, 'wysija_post_type_created', '1547655122', 'yes'),
(188, 'installation_step', '16', 'yes'),
(189, 'wysija', 'YToxODp7czo5OiJmcm9tX25hbWUiO3M6MTU6ImFtYW5kYXNjaGF1dGljYSI7czoxMjoicmVwbHl0b19uYW1lIjtzOjE1OiJhbWFuZGFzY2hhdXRpY2EiO3M6MTU6ImVtYWlsc19ub3RpZmllZCI7czozMToiZGV2aGNkZXNlbnZvbHZpbWVudG9zQGdtYWlsLmNvbSI7czoxMDoiZnJvbV9lbWFpbCI7czoxNDoiaW5mb0Bsb2NhbGhvc3QiO3M6MTM6InJlcGx5dG9fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjE1OiJkZWZhdWx0X2xpc3RfaWQiO2k6MTtzOjE3OiJ0b3RhbF9zdWJzY3JpYmVycyI7czoxOiIxIjtzOjE2OiJpbXBvcnR3cF9saXN0X2lkIjtpOjI7czoxODoiY29uZmlybV9lbWFpbF9saW5rIjtpOjIwO3M6MTI6InVwbG9hZGZvbGRlciI7czo3MDoiQzpcd2FtcDY0XHd3d1xwcm9qZXRvc1xhbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHNcd3lzaWphXCI7czo5OiJ1cGxvYWR1cmwiO3M6NzM6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS8iO3M6MTY6ImNvbmZpcm1fZW1haWxfaWQiO2k6MjtzOjk6Imluc3RhbGxlZCI7YjoxO3M6MjA6Im1hbmFnZV9zdWJzY3JpcHRpb25zIjtiOjE7czoxNDoiaW5zdGFsbGVkX3RpbWUiO2k6MTU0NzY1NTEyMztzOjE3OiJ3eXNpamFfZGJfdmVyc2lvbiI7czo2OiIyLjEwLjIiO3M6MTE6ImRraW1fZG9tYWluIjtzOjk6ImxvY2FsaG9zdCI7czoxNjoid3lzaWphX3doYXRzX25ldyI7czo2OiIyLjEwLjIiO30=', 'yes'),
(190, 'wysija_reinstall', '0', 'no'),
(191, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1548772343;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1547741527;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1548841507;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1548868257;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1550074327;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}}', 'yes'),
(192, 'wysija_queries', '', 'no'),
(193, 'wysija_queries_errors', '', 'no'),
(194, 'wysija_msg', '', 'no'),
(197, 'widget_views', 'a:2:{i:2;a:6:{s:5:\"title\";s:0:\"\";s:4:\"type\";s:11:\"most_viewed\";s:4:\"mode\";s:4:\"post\";s:5:\"limit\";i:3;s:5:\"chars\";i:200;s:7:\"cat_ids\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(198, 'wpp_settings_config', 'a:2:{s:5:\"stats\";a:7:{s:5:\"range\";s:9:\"last7days\";s:9:\"time_unit\";s:4:\"hour\";s:13:\"time_quantity\";i:24;s:8:\"order_by\";s:5:\"views\";s:5:\"limit\";i:10;s:9:\"post_type\";s:9:\"post,page\";s:9:\"freshness\";b:0;}s:5:\"tools\";a:7:{s:4:\"ajax\";b:0;s:3:\"css\";b:1;s:4:\"link\";a:1:{s:6:\"target\";s:5:\"_self\";}s:9:\"thumbnail\";a:4:{s:6:\"source\";s:8:\"featured\";s:5:\"field\";s:0:\"\";s:6:\"resize\";b:0;s:7:\"default\";s:0:\"\";}s:3:\"log\";a:3:{s:5:\"level\";i:1;s:5:\"limit\";i:0;s:13:\"expires_after\";i:180;}s:5:\"cache\";a:2:{s:6:\"active\";b:1;s:8:\"interval\";a:2:{s:4:\"time\";s:6:\"minute\";s:5:\"value\";i:1;}}s:8:\"sampling\";a:2:{s:6:\"active\";b:0;s:4:\"rate\";i:100;}}}', 'yes'),
(199, 'widget_wpp', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(200, 'wpp_ver', '4.2.2', 'yes'),
(201, 'wpp_rand', '2db234ef01d292ecffb902b76fa04dcf', 'yes'),
(204, 'wysija_last_php_cron_call', '1548768743', 'yes'),
(202, 'wysija_check_pn', '1548768742.954', 'yes'),
(203, 'wysija_last_scheduled_check', '1548768742', 'yes'),
(446, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1548768743;s:15:\"version_checked\";s:5:\"5.0.3\";s:12:\"translations\";a:0:{}}', 'no'),
(447, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1548768744;s:7:\"checked\";a:1:{s:15:\"amandaschautica\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(271, 'new_admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(280, 'disqus_sync_token', 'c886f89112e770e774015c213fb653c1', 'yes'),
(281, 'disqus_forum_url', 'amanda-schautica', 'yes'),
(282, 'disqus_public_key', 'cxPtAasOflSFNSSZLJMR0HjGcwwfDsCUVKcNyA9YHvAtVKI7oR0H1h5kAYj0ZOW3', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_popularpostsdata`
--

CREATE TABLE `as_popularpostsdata` (
  `postid` bigint(20) NOT NULL,
  `day` datetime NOT NULL,
  `last_viewed` datetime NOT NULL,
  `pageviews` bigint(20) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_popularpostsdata`
--

INSERT INTO `as_popularpostsdata` (`postid`, `day`, `last_viewed`, `pageviews`) VALUES
(7, '2019-01-16 15:39:31', '2019-01-16 15:57:21', 4),
(11, '2019-01-16 14:42:47', '2019-01-16 14:42:47', 1),
(16, '2019-01-16 15:59:23', '2019-01-27 21:07:06', 33),
(18, '2019-01-16 16:00:46', '2019-01-27 21:07:22', 11),
(30, '2019-01-21 14:18:22', '2019-01-22 14:20:13', 2),
(32, '2019-01-25 16:49:18', '2019-01-25 16:49:18', 1),
(33, '2019-01-17 10:49:24', '2019-01-17 11:05:10', 2),
(34, '2019-01-17 10:58:42', '2019-01-25 14:21:37', 4),
(35, '2019-01-17 10:51:31', '2019-01-17 10:52:22', 3),
(36, '2019-01-24 23:05:01', '2019-01-25 11:01:56', 3),
(37, '2019-01-25 14:33:11', '2019-01-25 14:34:37', 2),
(38, '2019-01-17 10:52:40', '2019-01-27 21:06:34', 21),
(49, '2019-01-25 11:51:00', '2019-01-27 21:07:17', 24);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_popularpostssummary`
--

CREATE TABLE `as_popularpostssummary` (
  `ID` bigint(20) NOT NULL,
  `postid` bigint(20) NOT NULL,
  `pageviews` bigint(20) NOT NULL DEFAULT '1',
  `view_date` date NOT NULL,
  `view_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_popularpostssummary`
--

INSERT INTO `as_popularpostssummary` (`ID`, `postid`, `pageviews`, `view_date`, `view_datetime`) VALUES
(1, 11, 1, '2019-01-16', '2019-01-16 14:42:47'),
(2, 7, 1, '2019-01-16', '2019-01-16 15:39:31'),
(3, 7, 1, '2019-01-16', '2019-01-16 15:41:58'),
(4, 7, 1, '2019-01-16', '2019-01-16 15:56:55'),
(5, 7, 1, '2019-01-16', '2019-01-16 15:57:21'),
(6, 16, 1, '2019-01-16', '2019-01-16 15:59:23'),
(7, 18, 1, '2019-01-16', '2019-01-16 16:00:46'),
(8, 16, 1, '2019-01-17', '2019-01-17 09:12:06'),
(9, 16, 1, '2019-01-17', '2019-01-17 09:16:49'),
(10, 16, 1, '2019-01-17', '2019-01-17 09:23:21'),
(11, 16, 1, '2019-01-17', '2019-01-17 09:24:26'),
(12, 16, 1, '2019-01-17', '2019-01-17 09:24:40'),
(13, 16, 1, '2019-01-17', '2019-01-17 09:25:43'),
(14, 16, 1, '2019-01-17', '2019-01-17 09:26:05'),
(15, 16, 1, '2019-01-17', '2019-01-17 09:26:35'),
(16, 16, 1, '2019-01-17', '2019-01-17 09:26:43'),
(17, 16, 1, '2019-01-17', '2019-01-17 09:27:01'),
(18, 16, 1, '2019-01-17', '2019-01-17 09:28:02'),
(19, 16, 1, '2019-01-17', '2019-01-17 09:28:42'),
(20, 16, 1, '2019-01-17', '2019-01-17 09:29:21'),
(21, 16, 1, '2019-01-17', '2019-01-17 09:30:37'),
(22, 16, 1, '2019-01-17', '2019-01-17 09:31:55'),
(23, 16, 1, '2019-01-17', '2019-01-17 09:33:34'),
(24, 16, 1, '2019-01-17', '2019-01-17 09:34:05'),
(25, 16, 1, '2019-01-17', '2019-01-17 09:34:22'),
(26, 16, 1, '2019-01-17', '2019-01-17 09:34:44'),
(27, 16, 1, '2019-01-17', '2019-01-17 09:36:52'),
(28, 16, 1, '2019-01-17', '2019-01-17 09:37:29'),
(29, 16, 1, '2019-01-17', '2019-01-17 09:38:21'),
(30, 16, 1, '2019-01-17', '2019-01-17 09:43:29'),
(31, 16, 1, '2019-01-17', '2019-01-17 09:45:34'),
(32, 16, 1, '2019-01-17', '2019-01-17 09:46:01'),
(33, 18, 1, '2019-01-17', '2019-01-17 09:46:46'),
(34, 18, 1, '2019-01-17', '2019-01-17 09:47:41'),
(35, 18, 1, '2019-01-17', '2019-01-17 09:49:20'),
(36, 18, 1, '2019-01-17', '2019-01-17 09:49:48'),
(37, 16, 1, '2019-01-17', '2019-01-17 10:49:03'),
(38, 18, 1, '2019-01-17', '2019-01-17 10:49:14'),
(39, 33, 1, '2019-01-17', '2019-01-17 10:49:24'),
(40, 35, 1, '2019-01-17', '2019-01-17 10:51:31'),
(41, 35, 1, '2019-01-17', '2019-01-17 10:52:16'),
(42, 35, 1, '2019-01-17', '2019-01-17 10:52:22'),
(43, 38, 1, '2019-01-17', '2019-01-17 10:52:40'),
(44, 38, 1, '2019-01-17', '2019-01-17 10:56:19'),
(45, 34, 1, '2019-01-17', '2019-01-17 10:58:42'),
(46, 16, 1, '2019-01-17', '2019-01-17 10:58:57'),
(47, 18, 1, '2019-01-17', '2019-01-17 10:59:08'),
(48, 33, 1, '2019-01-17', '2019-01-17 11:05:10'),
(49, 18, 1, '2019-01-17', '2019-01-17 11:27:12'),
(50, 38, 1, '2019-01-17', '2019-01-17 15:42:53'),
(51, 38, 1, '2019-01-17', '2019-01-17 15:44:41'),
(52, 38, 1, '2019-01-17', '2019-01-17 15:47:23'),
(53, 38, 1, '2019-01-17', '2019-01-17 15:47:33'),
(54, 38, 1, '2019-01-17', '2019-01-17 15:48:10'),
(55, 38, 1, '2019-01-17', '2019-01-17 15:49:20'),
(56, 38, 1, '2019-01-17', '2019-01-17 15:54:05'),
(57, 38, 1, '2019-01-17', '2019-01-17 15:54:55'),
(58, 38, 1, '2019-01-17', '2019-01-17 15:55:19'),
(59, 38, 1, '2019-01-17', '2019-01-17 15:55:23'),
(60, 38, 1, '2019-01-17', '2019-01-17 15:57:12'),
(61, 38, 1, '2019-01-17', '2019-01-17 15:58:21'),
(62, 38, 1, '2019-01-17', '2019-01-17 15:59:38'),
(63, 16, 1, '2019-01-18', '2019-01-18 09:20:45'),
(64, 18, 1, '2019-01-18', '2019-01-18 09:21:07'),
(65, 38, 1, '2019-01-18', '2019-01-18 09:29:33'),
(66, 18, 1, '2019-01-18', '2019-01-18 09:42:25'),
(67, 16, 1, '2019-01-18', '2019-01-18 09:42:33'),
(68, 38, 1, '2019-01-18', '2019-01-18 10:22:32'),
(69, 38, 1, '2019-01-18', '2019-01-18 10:22:48'),
(70, 38, 1, '2019-01-21', '2019-01-21 12:40:17'),
(71, 30, 1, '2019-01-21', '2019-01-21 14:18:22'),
(72, 30, 1, '2019-01-22', '2019-01-22 14:20:13'),
(73, 38, 1, '2019-01-22', '2019-01-22 14:20:23'),
(74, 34, 1, '2019-01-23', '2019-01-23 16:26:23'),
(75, 36, 1, '2019-01-24', '2019-01-24 23:05:01'),
(76, 36, 1, '2019-01-25', '2019-01-25 10:05:00'),
(77, 36, 1, '2019-01-25', '2019-01-25 11:01:56'),
(78, 34, 1, '2019-01-25', '2019-01-25 11:07:15'),
(79, 16, 1, '2019-01-25', '2019-01-25 11:42:09'),
(80, 49, 1, '2019-01-25', '2019-01-25 11:51:00'),
(81, 49, 1, '2019-01-25', '2019-01-25 11:55:05'),
(82, 49, 1, '2019-01-25', '2019-01-25 11:55:25'),
(83, 49, 1, '2019-01-25', '2019-01-25 11:55:36'),
(84, 49, 1, '2019-01-25', '2019-01-25 11:55:50'),
(85, 49, 1, '2019-01-25', '2019-01-25 12:02:27'),
(86, 49, 1, '2019-01-25', '2019-01-25 12:03:27'),
(87, 49, 1, '2019-01-25', '2019-01-25 12:05:31'),
(88, 49, 1, '2019-01-25', '2019-01-25 12:07:38'),
(89, 49, 1, '2019-01-25', '2019-01-25 12:15:58'),
(90, 49, 1, '2019-01-25', '2019-01-25 12:17:02'),
(91, 49, 1, '2019-01-25', '2019-01-25 12:20:07'),
(92, 49, 1, '2019-01-25', '2019-01-25 12:20:14'),
(93, 49, 1, '2019-01-25', '2019-01-25 12:28:18'),
(94, 49, 1, '2019-01-25', '2019-01-25 12:30:07'),
(95, 49, 1, '2019-01-25', '2019-01-25 12:31:31'),
(96, 49, 1, '2019-01-25', '2019-01-25 12:33:02'),
(97, 49, 1, '2019-01-25', '2019-01-25 12:33:28'),
(98, 49, 1, '2019-01-25', '2019-01-25 12:34:35'),
(99, 49, 1, '2019-01-25', '2019-01-25 12:35:26'),
(100, 49, 1, '2019-01-25', '2019-01-25 12:35:51'),
(101, 49, 1, '2019-01-25', '2019-01-25 13:47:42'),
(102, 34, 1, '2019-01-25', '2019-01-25 14:21:37'),
(103, 37, 1, '2019-01-25', '2019-01-25 14:33:11'),
(104, 37, 1, '2019-01-25', '2019-01-25 14:34:37'),
(105, 32, 1, '2019-01-25', '2019-01-25 16:49:18'),
(106, 49, 1, '2019-01-25', '2019-01-25 16:50:08'),
(107, 16, 1, '2019-01-25', '2019-01-25 16:50:21'),
(108, 38, 1, '2019-01-27', '2019-01-27 21:06:34'),
(109, 16, 1, '2019-01-27', '2019-01-27 21:07:06'),
(110, 49, 1, '2019-01-27', '2019-01-27 21:07:17'),
(111, 18, 1, '2019-01-27', '2019-01-27 21:07:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_postmeta`
--

CREATE TABLE `as_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_postmeta`
--

INSERT INTO `as_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2019/01/Amanda-Schautica.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:529;s:6:\"height\";i:284;s:4:\"file\";s:28:\"2019/01/Amanda-Schautica.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"Amanda-Schautica-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"Amanda-Schautica-300x161.png\";s:5:\"width\";i:300;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 6, '_wp_attached_file', '2019/01/Amanda-Schautica2.png'),
(6, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:529;s:6:\"height\";i:284;s:4:\"file\";s:29:\"2019/01/Amanda-Schautica2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"Amanda-Schautica2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"Amanda-Schautica2-300x161.png\";s:5:\"width\";i:300;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(7, 7, '_edit_lock', '1547654349:1'),
(8, 8, '_wp_attached_file', '2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png'),
(9, 8, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:450;s:4:\"file\";s:36:\"2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"1_0qPOXPqrYVpM6tUSYTK7SQ-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"1_0qPOXPqrYVpM6tUSYTK7SQ-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 10, '_wp_attached_file', '2019/01/1_5CBgnQWE2fgaJzoiJ4IUUg.jpeg'),
(12, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:675;s:4:\"file\";s:37:\"2019/01/1_5CBgnQWE2fgaJzoiJ4IUUg.jpeg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1_5CBgnQWE2fgaJzoiJ4IUUg-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1_5CBgnQWE2fgaJzoiJ4IUUg-300x169.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1_5CBgnQWE2fgaJzoiJ4IUUg-768x432.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1_5CBgnQWE2fgaJzoiJ4IUUg-1024x576.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(16, 11, '_edit_lock', '1547726089:1'),
(14, 7, '_thumbnail_id', '10'),
(17, 12, '_wp_attached_file', '2019/01/1_Jmcm44hTrhIM1HI5n_st5g.jpeg'),
(18, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:730;s:4:\"file\";s:37:\"2019/01/1_Jmcm44hTrhIM1HI5n_st5g.jpeg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1_Jmcm44hTrhIM1HI5n_st5g-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1_Jmcm44hTrhIM1HI5n_st5g-300x183.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:183;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1_Jmcm44hTrhIM1HI5n_st5g-768x467.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:467;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:623;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 13, '_wp_attached_file', '2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png'),
(20, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:450;s:4:\"file\";s:38:\"2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"1_0qPOXPqrYVpM6tUSYTK7SQ-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"1_0qPOXPqrYVpM6tUSYTK7SQ-1-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(24, 1, '_wp_trash_meta_status', 'publish'),
(22, 11, '_thumbnail_id', '12'),
(25, 1, '_wp_trash_meta_time', '1547647058'),
(26, 1, '_wp_desired_post_slug', 'ola-mundo'),
(27, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(29, 16, '_edit_lock', '1547724152:1'),
(30, 16, '_wp_page_template', 'pg-templates/contato.php'),
(31, 18, '_edit_lock', '1547725772:1'),
(32, 18, '_wp_page_template', 'pg-templates/quemSomos.php'),
(33, 7, 'views', '1'),
(34, 11, 'views', '2'),
(35, 16, 'views', '31'),
(36, 16, '_edit_last', '1'),
(37, 23, '_wp_attached_file', '2019/01/assorted-blur-close-up-1115128.jpg'),
(38, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:5184;s:6:\"height\";i:3456;s:4:\"file\";s:42:\"2019/01/assorted-blur-close-up-1115128.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"assorted-blur-close-up-1115128-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:42:\"assorted-blur-close-up-1115128-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:42:\"assorted-blur-close-up-1115128-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:43:\"assorted-blur-close-up-1115128-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:42:\"assorted-blur-close-up-1115128-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 16, '_thumbnail_id', '23'),
(40, 26, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(41, 26, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:33:\"Amanda Schautica \"[your-subject]\"\";s:6:\"sender\";s:50:\"Amanda Schautica <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:201:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(42, 26, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:33:\"Amanda Schautica \"[your-subject]\"\";s:6:\"sender\";s:50:\"Amanda Schautica <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:143:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(43, 26, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:48:\"Obrigado pela sua mensagem. Ela já foi enviada.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:72:\"Houve um erro ao tentar enviar sua mensagem. Por favor, tente novamente.\";s:4:\"spam\";s:83:\"Houve um erro ao tentar enviar sua mensagem. Por favor, tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato da data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:41:\"A data é posterior à última permitida.\";s:13:\"upload_failed\";s:56:\"Houve um erro desconhecido ao fazer o upload do arquivo.\";s:24:\"upload_file_type_invalid\";s:67:\"Você não tem permissão para fazer upload de arquivos desse tipo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:34:\"Houve um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:33:\"O formato numérico é inválido.\";s:16:\"number_too_small\";s:43:\"O número é menor que o mínimo permitido.\";s:16:\"number_too_large\";s:43:\"O número é maior que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:48:\"A resposta para o questionário está incorreta.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:44:\"O endereço de e-mail digitado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválida.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(44, 26, '_additional_settings', ''),
(45, 26, '_locale', 'pt_BR'),
(46, 27, '_form', '[text* nome id:nome class:nome placeholder \"Nome\"][email* email id:email class:email placeholder \"E-mail\"][text assunto id:assunto class:assunto placeholder \"Assunto\"][textarea mensagem id:mensagem class:mensagem placeholder \"mensagem...\"][submit id:Enviar class:enviar \"Enviar\"]'),
(47, 27, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:28:\"Amanda Schautica \"[assunto]\"\";s:6:\"sender\";s:50:\"Amanda Schautica <wordpress@amandakaroline.com.br>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:187:\"From: [nome] <[email]>\nSubject: [your-subject]\n\nMessage Body:\n[mensagem]\n\n-- \nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(48, 27, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:33:\"Amanda Schautica \"[your-subject]\"\";s:6:\"sender\";s:50:\"Amanda Schautica <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:143:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(49, 27, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(50, 27, '_additional_settings', ''),
(51, 27, '_locale', 'pt_BR'),
(127, 56, '_menu_item_type', 'post_type'),
(128, 56, '_menu_item_menu_item_parent', '0'),
(129, 56, '_menu_item_object_id', '49'),
(130, 56, '_menu_item_object', 'page'),
(131, 56, '_menu_item_target', ''),
(53, 18, 'views', '10'),
(54, 2, '_wp_trash_meta_status', 'publish'),
(55, 2, '_wp_trash_meta_time', '1547725708'),
(56, 2, '_wp_desired_post_slug', 'pagina-exemplo'),
(57, 18, '_thumbnail_id', '23'),
(59, 30, '_thumbnail_id', '10'),
(60, 30, 'views', '7'),
(61, 30, '_dp_original', '7'),
(62, 31, '_thumbnail_id', '12'),
(63, 31, 'views', '2'),
(64, 31, '_dp_original', '11'),
(65, 32, '_thumbnail_id', '10'),
(66, 32, 'views', '3'),
(67, 32, '_dp_original', '7'),
(68, 33, '_thumbnail_id', '12'),
(69, 33, 'views', '5'),
(70, 33, '_dp_original', '11'),
(71, 34, '_thumbnail_id', '10'),
(72, 34, 'views', '6'),
(73, 34, '_dp_original', '7'),
(74, 35, '_thumbnail_id', '12'),
(75, 35, 'views', '2'),
(76, 35, '_dp_original', '11'),
(77, 36, '_thumbnail_id', '10'),
(78, 36, 'views', '5'),
(79, 36, '_dp_original', '7'),
(80, 37, '_thumbnail_id', '12'),
(81, 37, 'views', '3'),
(82, 37, '_dp_original', '11'),
(83, 38, '_thumbnail_id', '10'),
(84, 38, 'views', '25'),
(85, 38, '_dp_original', '7'),
(86, 38, '_edit_last', '1'),
(89, 37, '_edit_last', '1'),
(88, 38, '_edit_lock', '1548092311:1'),
(92, 35, '_edit_last', '1'),
(91, 37, '_edit_lock', '1547726951:1'),
(107, 31, '_edit_last', '1'),
(94, 35, '_edit_lock', '1547726958:1'),
(95, 36, '_edit_last', '1'),
(98, 34, '_edit_last', '1'),
(97, 36, '_edit_lock', '1547726962:1'),
(100, 34, '_edit_lock', '1547726968:1'),
(101, 32, '_edit_last', '1'),
(104, 33, '_edit_last', '1'),
(103, 32, '_edit_lock', '1547726974:1'),
(106, 33, '_edit_lock', '1547726979:1'),
(109, 31, '_edit_lock', '1547726984:1'),
(110, 30, '_edit_last', '1'),
(112, 30, '_edit_lock', '1547726994:1'),
(115, 49, '_edit_lock', '1548431206:1'),
(116, 49, 'views', '3'),
(117, 49, '_wp_page_template', 'pg-templates/anuncie.php'),
(118, 53, '_form', '[text* nome id:nome placeholder \"Nome\"][email* email id:email placeholder \"E-mail\"][text assunto id:assunto placeholder \"Assunto\"][textarea mensagem id:mensagem placeholder \"mensagem...\"][submit id:enviar \"Enviar\"]'),
(119, 53, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:28:\"Amanda Schautica \"[assunto]\"\";s:6:\"sender\";s:45:\"Amanda Schautica <info@amandakaroline.com.br>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:196:\"De: [nome] <[email]>\nAssunto: [assunto]\n\nCorpo da mensagem:\n[mensagem]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Amanda Schautica (https://amandakaroline.com.br/amandaschautica)\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(120, 53, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:33:\"Amanda Schautica \"[your-subject]\"\";s:6:\"sender\";s:50:\"Amanda Schautica <wordpress@amandakaroline.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:159:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Amanda Schautica (https://amandakaroline.com.br/amandaschautica)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(121, 53, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:63:\"Um ou mais campos possuem um erro. Verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e-mail informado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválida.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(122, 53, '_additional_settings', ''),
(123, 53, '_locale', 'pt_BR'),
(132, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(133, 56, '_menu_item_xfn', ''),
(134, 56, '_menu_item_url', ''),
(156, 59, '_menu_item_object_id', '2'),
(136, 57, '_menu_item_type', 'post_type'),
(137, 57, '_menu_item_menu_item_parent', '0'),
(138, 57, '_menu_item_object_id', '18'),
(139, 57, '_menu_item_object', 'page'),
(140, 57, '_menu_item_target', ''),
(141, 57, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(142, 57, '_menu_item_xfn', ''),
(143, 57, '_menu_item_url', ''),
(155, 59, '_menu_item_menu_item_parent', '0'),
(145, 58, '_menu_item_type', 'post_type'),
(146, 58, '_menu_item_menu_item_parent', '0'),
(147, 58, '_menu_item_object_id', '16'),
(148, 58, '_menu_item_object', 'page'),
(149, 58, '_menu_item_target', ''),
(150, 58, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(151, 58, '_menu_item_xfn', ''),
(152, 58, '_menu_item_url', ''),
(154, 59, '_menu_item_type', 'taxonomy'),
(157, 59, '_menu_item_object', 'category'),
(158, 59, '_menu_item_target', ''),
(159, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(160, 59, '_menu_item_xfn', ''),
(161, 59, '_menu_item_url', ''),
(163, 60, '_menu_item_type', 'taxonomy'),
(164, 60, '_menu_item_menu_item_parent', '0'),
(165, 60, '_menu_item_object_id', '7'),
(166, 60, '_menu_item_object', 'category'),
(167, 60, '_menu_item_target', ''),
(168, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(169, 60, '_menu_item_xfn', ''),
(170, 60, '_menu_item_url', ''),
(200, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:359;s:6:\"height\";i:166;s:4:\"file\";s:20:\"2019/01/LOGO-PNG.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"LOGO-PNG-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"LOGO-PNG-300x139.png\";s:5:\"width\";i:300;s:6:\"height\";i:139;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(172, 61, '_menu_item_type', 'taxonomy'),
(173, 61, '_menu_item_menu_item_parent', '0'),
(174, 61, '_menu_item_object_id', '8'),
(175, 61, '_menu_item_object', 'category'),
(176, 61, '_menu_item_target', ''),
(177, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(178, 61, '_menu_item_xfn', ''),
(179, 61, '_menu_item_url', ''),
(181, 62, '_menu_item_type', 'taxonomy'),
(182, 62, '_menu_item_menu_item_parent', '0'),
(183, 62, '_menu_item_object_id', '10'),
(184, 62, '_menu_item_object', 'category'),
(185, 62, '_menu_item_target', ''),
(186, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(187, 62, '_menu_item_xfn', ''),
(188, 62, '_menu_item_url', ''),
(199, 64, '_wp_attached_file', '2019/01/LOGO-PNG.png'),
(190, 63, '_menu_item_type', 'custom'),
(191, 63, '_menu_item_menu_item_parent', '0'),
(192, 63, '_menu_item_object_id', '63'),
(193, 63, '_menu_item_object', 'custom'),
(194, 63, '_menu_item_target', ''),
(195, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(196, 63, '_menu_item_xfn', ''),
(197, 63, '_menu_item_url', 'https://amandakaroline.com.br/amandaschautica/');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_posts`
--

CREATE TABLE `as_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_posts`
--

INSERT INTO `as_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-01-16 11:14:07', '2019-01-16 13:14:07', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'trash', 'open', 'open', '', 'ola-mundo__trashed', '', '', '2019-01-16 11:57:38', '2019-01-16 13:57:38', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=1', 0, 'post', '', 1),
(2, 1, '2019-01-16 11:14:07', '2019-01-16 13:14:07', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/amandaschautica_blog/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'trash', 'closed', 'open', '', 'pagina-exemplo__trashed', '', '', '2019-01-17 09:48:28', '2019-01-17 11:48:28', '', 0, 'http://localhost/projetos/amandaschautica_blog/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-01-16 11:14:07', '2019-01-16 13:14:07', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/amandaschautica_blog.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-01-16 11:14:07', '2019-01-16 13:14:07', '', 0, 'http://localhost/projetos/amandaschautica_blog/?page_id=3', 0, 'page', '', 0),
(48, 1, '2019-01-25 11:36:27', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-01-25 11:36:27', '0000-00-00 00:00:00', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=48', 0, 'post', '', 0),
(5, 1, '2019-01-16 11:44:26', '2019-01-16 13:44:26', '', 'Amanda-Schautica', '', 'inherit', 'open', 'closed', '', 'amanda-schautica', '', '', '2019-01-16 11:44:26', '2019-01-16 13:44:26', '', 0, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/Amanda-Schautica.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2019-01-16 11:46:05', '2019-01-16 13:46:05', '', 'Amanda-Schautica(2)', '', 'inherit', 'open', 'closed', '', 'amanda-schautica2', '', '', '2019-01-16 11:46:05', '2019-01-16 13:46:05', '', 0, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/Amanda-Schautica2.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2019-01-16 11:52:49', '2019-01-16 13:52:49', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe', '', '', '2019-01-16 11:58:03', '2019-01-16 13:58:03', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=7', 0, 'post', '', 0),
(8, 1, '2019-01-16 11:52:00', '2019-01-16 13:52:00', '', '1_0qPOXPqrYVpM6tUSYTK7SQ', '', 'inherit', 'open', 'closed', '', '1_0qpoxpqryvpm6tusytk7sq', '', '', '2019-01-16 11:52:00', '2019-01-16 13:52:00', '', 7, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png', 0, 'attachment', 'image/png', 0),
(9, 1, '2019-01-16 11:52:49', '2019-01-16 13:52:49', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-01-16 11:52:49', '2019-01-16 13:52:49', '', 7, 'http://localhost/projetos/amandaschautica_blog/2019/01/16/7-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-01-16 11:52:59', '2019-01-16 13:52:59', '', '1_5CBgnQWE2fgaJzoiJ4IUUg', '', 'inherit', 'open', 'closed', '', '1_5cbgnqwe2fgajzoij4iuug', '', '', '2019-01-16 11:52:59', '2019-01-16 13:52:59', '', 7, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_5CBgnQWE2fgaJzoiJ4IUUg.jpeg', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2019-01-16 11:56:07', '2019-01-16 13:56:07', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'publish', 'open', 'open', '', 'boas-opcoes-de-shampoo-para-o-seu-dia-a-dia-e-tipo-de-cabelo', '', '', '2019-01-17 09:50:20', '2019-01-17 11:50:20', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=11', 0, 'post', '', 0),
(12, 1, '2019-01-16 11:55:51', '2019-01-16 13:55:51', '', '1_Jmcm44hTrhIM1HI5n_st5g', '', 'inherit', 'open', 'closed', '', '1_jmcm44htrhim1hi5n_st5g', '', '', '2019-01-16 11:55:51', '2019-01-16 13:55:51', '', 11, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g.jpeg', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2019-01-16 11:55:59', '2019-01-16 13:55:59', '', '1_0qPOXPqrYVpM6tUSYTK7SQ', '', 'inherit', 'open', 'closed', '', '1_0qpoxpqryvpm6tusytk7sq-2', '', '', '2019-01-16 11:55:59', '2019-01-16 13:55:59', '', 11, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png', 0, 'attachment', 'image/png', 0),
(14, 1, '2019-01-16 11:56:07', '2019-01-16 13:56:07', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-01-16 11:56:07', '2019-01-16 13:56:07', '', 11, 'http://localhost/projetos/amandaschautica_blog/2019/01/16/11-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2019-01-16 11:57:38', '2019-01-16 13:57:38', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-16 11:57:38', '2019-01-16 13:57:38', '', 1, 'http://localhost/projetos/amandaschautica_blog/2019/01/16/1-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-01-16 12:05:35', '2019-01-16 14:05:35', '<!-- wp:heading {\"level\":3} -->\n<h3>Entre em contato</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit\n\n</p>\n<!-- /wp:paragraph -->', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-01-17 09:23:16', '2019-01-17 11:23:16', '', 0, 'http://localhost/projetos/amandaschautica_blog/?page_id=16', 0, 'page', '', 0),
(17, 1, '2019-01-16 12:05:35', '2019-01-16 14:05:35', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-01-16 12:05:35', '2019-01-16 14:05:35', '', 16, 'http://localhost/projetos/amandaschautica_blog/2019/01/16/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-01-16 12:05:48', '2019-01-16 14:05:48', '<!-- wp:paragraph -->\n<p> Cras facilisis ut quam in porta. Donec sit amet est eget mi ultricies imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris quis vehicula nibh. Proin leo tellus, finibus in venenatis eget, aliquet vitae orci. Ut volutpat libero sed luctus pre.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Mauris faucibus quis orci id tincidunt. Suspendisse congue dignissim urna, posuere lobortis nibh imperdiet id. Sed id imperdiet nunc. Sed nisl tellus, scelerisque at porta non, efficitur blandit dui. In vestibulum sapien non consectetur mollis. Mauris id feugiat leo. Aenean ut metus lorem.  </p>\n<!-- /wp:paragraph -->', 'Sobre', '', 'publish', 'closed', 'closed', '', 'sobre', '', '', '2019-01-17 09:49:16', '2019-01-17 11:49:16', '', 0, 'http://localhost/projetos/amandaschautica_blog/?page_id=18', 0, 'page', '', 0),
(19, 1, '2019-01-16 12:05:48', '2019-01-16 14:05:48', '', 'Sobre', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-01-16 12:05:48', '2019-01-16 14:05:48', '', 18, 'http://localhost/projetos/amandaschautica_blog/2019/01/16/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-01-16 14:12:03', '2019-01-16 16:12:03', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2019-01-16 14:12:03', '2019-01-16 16:12:03', '', 0, 'http://localhost/projetos/amandaschautica_blog/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(23, 1, '2019-01-17 09:21:57', '2019-01-17 11:21:57', '', 'assorted-blur-close-up-1115128', '', 'inherit', 'open', 'closed', '', 'assorted-blur-close-up-1115128', '', '', '2019-01-17 09:21:57', '2019-01-17 11:21:57', '', 16, 'http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/assorted-blur-close-up-1115128.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2019-01-17 09:22:59', '2019-01-17 11:22:59', '<!-- wp:heading {\"level\":3} -->\n<h3>Entre em contato</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit sapien vulputate. Praesent ex eros, accumsan vitae felis eleifend, sodales fringilla ipsum. Etiam quis molestie diam, vel sodales ipsum. Etiam at dolor libero. Aenean vitae tempus erat. Sed lobortis bibendum pulvinar. Sed sodales quam eget fermentum porta. Fusce nulla lorem, faucibus vel viverra congue, molestie at est. Suspendisse diam libero, dapibus sit amet tortor quis, finibus hendrerit ante.\n\n</p>\n<!-- /wp:paragraph -->', 'Contato', '', 'inherit', 'closed', 'closed', '', '16-autosave-v1', '', '', '2019-01-17 09:22:59', '2019-01-17 11:22:59', '', 16, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/16-autosave-v1/', 0, 'revision', '', 0),
(22, 1, '2019-01-17 09:21:49', '2019-01-17 11:21:49', '<!-- wp:heading {\"level\":3} -->\n<h3>Entre em contato</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-01-17 09:21:49', '2019-01-17 11:21:49', '', 16, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/16-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2019-01-17 09:23:16', '2019-01-17 11:23:16', '<!-- wp:heading {\"level\":3} -->\n<h3>Entre em contato</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor ullamcorper ipsum, eu feugiat ante ultricies nec. Duis semper velit ut ligula venenatis, in hendrerit\n\n</p>\n<!-- /wp:paragraph -->', 'Contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-01-17 09:23:16', '2019-01-17 11:23:16', '', 16, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/16-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2019-01-17 09:25:01', '2019-01-17 11:25:01', '<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n[submit \"Send\"]\n1\nAmanda Schautica \"[your-subject]\"\nAmanda Schautica <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\nReply-To: [your-email]\n\n\n\n\nAmanda Schautica \"[your-subject]\"\nAmanda Schautica <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nObrigado pela sua mensagem. Ela já foi enviada.\nThere was an error trying to send your message. Please try again later.\nHouve um erro ao tentar enviar sua mensagem. Por favor, tente novamente.\nHouve um erro ao tentar enviar sua mensagem. Por favor, tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato da data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à última permitida.\nHouve um erro desconhecido ao fazer o upload do arquivo.\nVocê não tem permissão para fazer upload de arquivos desse tipo.\nO arquivo é muito grande.\nHouve um erro ao enviar o arquivo.\nO formato numérico é inválido.\nO número é menor que o mínimo permitido.\nO número é maior que o máximo permitido.\nA resposta para o questionário está incorreta.\nYour entered code is incorrect.\nO endereço de e-mail digitado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-01-17 09:43:20', '2019-01-17 11:43:20', '', 0, 'http://localhost/projetos/amandaschautica_blog/?post_type=wpcf7_contact_form&#038;p=26', 0, 'wpcf7_contact_form', '', 0),
(27, 1, '2019-01-17 09:25:37', '2019-01-17 11:25:37', '[text* nome id:nome class:nome placeholder \"Nome\"][email* email id:email class:email placeholder \"E-mail\"][text assunto id:assunto class:assunto placeholder \"Assunto\"][textarea mensagem id:mensagem class:mensagem placeholder \"mensagem...\"][submit id:Enviar class:enviar \"Enviar\"]\n1\nAmanda Schautica \"[assunto]\"\nAmanda Schautica <wordpress@amandakaroline.com.br>\ndevhcdesenvolvimentos@gmail.com\nFrom: [nome] <[email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[mensagem]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\nReply-To: [email]\n\n\n\n\nAmanda Schautica \"[your-subject]\"\nAmanda Schautica <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Amanda Schautica (http://localhost/projetos/amandaschautica_blog)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Formulário de contato', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato', '', '', '2019-01-25 13:52:53', '2019-01-25 15:52:53', '', 0, 'http://localhost/projetos/amandaschautica_blog/?post_type=wpcf7_contact_form&#038;p=27', 0, 'wpcf7_contact_form', '', 0),
(28, 1, '2019-01-17 09:48:28', '2019-01-17 11:48:28', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/amandaschautica_blog/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-01-17 09:48:28', '2019-01-17 11:48:28', '', 2, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/2-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2019-01-17 09:49:16', '2019-01-17 11:49:16', '<!-- wp:paragraph -->\n<p> Cras facilisis ut quam in porta. Donec sit amet est eget mi ultricies imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris quis vehicula nibh. Proin leo tellus, finibus in venenatis eget, aliquet vitae orci. Ut volutpat libero sed luctus pre.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Mauris faucibus quis orci id tincidunt. Suspendisse congue dignissim urna, posuere lobortis nibh imperdiet id. Sed id imperdiet nunc. Sed nisl tellus, scelerisque at porta non, efficitur blandit dui. In vestibulum sapien non consectetur mollis. Mauris id feugiat leo. Aenean ut metus lorem.  </p>\n<!-- /wp:paragraph -->', 'Sobre', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-01-17 09:49:16', '2019-01-17 11:49:16', '', 18, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/18-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2019-01-17 10:08:49', '2019-01-17 12:08:49', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe-6', '', '', '2019-01-17 10:09:54', '2019-01-17 12:09:54', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=30', 0, 'post', '', 0),
(31, 1, '2019-01-17 10:08:50', '2019-01-17 12:08:50', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'publish', 'open', 'open', '', 'boas-opcoes-de-shampoo-para-o-seu-dia-a-dia-e-tipo-de-cabelo-5', '', '', '2019-01-17 10:09:44', '2019-01-17 12:09:44', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=31', 0, 'post', '', 0),
(32, 1, '2019-01-17 10:08:51', '2019-01-17 12:08:51', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe-5', '', '', '2019-01-17 10:09:34', '2019-01-17 12:09:34', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=32', 0, 'post', '', 0),
(33, 1, '2019-01-17 10:08:51', '2019-01-17 12:08:51', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'publish', 'open', 'open', '', 'boas-opcoes-de-shampoo-para-o-seu-dia-a-dia-e-tipo-de-cabelo-4', '', '', '2019-01-17 10:09:39', '2019-01-17 12:09:39', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=33', 0, 'post', '', 0);
INSERT INTO `as_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(34, 1, '2019-01-17 10:08:52', '2019-01-17 12:08:52', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe-4', '', '', '2019-01-17 10:09:28', '2019-01-17 12:09:28', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=34', 0, 'post', '', 0),
(35, 1, '2019-01-17 10:08:53', '2019-01-17 12:08:53', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'publish', 'open', 'open', '', 'boas-opcoes-de-shampoo-para-o-seu-dia-a-dia-e-tipo-de-cabelo-3', '', '', '2019-01-17 10:09:18', '2019-01-17 12:09:18', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=35', 0, 'post', '', 0),
(36, 1, '2019-01-17 10:08:53', '2019-01-17 12:08:53', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe-3', '', '', '2019-01-17 10:09:22', '2019-01-17 12:09:22', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=36', 0, 'post', '', 0),
(37, 1, '2019-01-17 10:08:54', '2019-01-17 12:08:54', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'publish', 'open', 'open', '', 'boas-opcoes-de-shampoo-para-o-seu-dia-a-dia-e-tipo-de-cabelo-2', '', '', '2019-01-17 10:09:11', '2019-01-17 12:09:11', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=37', 0, 'post', '', 0),
(38, 1, '2019-01-17 10:08:55', '2019-01-17 12:08:55', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'publish', 'open', 'open', '', 'dicas-e-truques-de-maquiagem-que-ninguem-sabe-2', '', '', '2019-01-21 15:27:16', '2019-01-21 17:27:16', '', 0, 'http://localhost/projetos/amandaschautica_blog/?p=38', 0, 'post', '', 0),
(39, 1, '2019-01-17 10:09:02', '2019-01-17 12:09:02', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2019-01-17 10:09:02', '2019-01-17 12:09:02', '', 38, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/38-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2019-01-17 10:09:11', '2019-01-17 12:09:11', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2019-01-17 10:09:11', '2019-01-17 12:09:11', '', 37, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/37-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2019-01-17 10:09:18', '2019-01-17 12:09:18', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2019-01-17 10:09:18', '2019-01-17 12:09:18', '', 35, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/35-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2019-01-17 10:09:22', '2019-01-17 12:09:22', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2019-01-17 10:09:22', '2019-01-17 12:09:22', '', 36, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/36-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2019-01-17 10:09:28', '2019-01-17 12:09:28', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-01-17 10:09:28', '2019-01-17 12:09:28', '', 34, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/34-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2019-01-17 10:09:34', '2019-01-17 12:09:34', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2019-01-17 10:09:34', '2019-01-17 12:09:34', '', 32, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/32-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2019-01-17 10:09:39', '2019-01-17 12:09:39', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2019-01-17 10:09:39', '2019-01-17 12:09:39', '', 33, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/33-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2019-01-17 10:09:44', '2019-01-17 12:09:44', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"bcb8\">Ter os cabelos limpos, soltos e sedosos todo dia começa sempre pelo hábito da sua lavagem.&nbsp;💆</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Com o seu ritual de<strong>&nbsp;shampoo + condicionador (ou máscara)</strong>&nbsp;no dia a dia, você consegue frequentemente \"entregar\" ao seu cabelo os nutrientes e hidratação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"1f96\"><strong>Porém, achar um shampoo realmente BOM é bem mais relativo:</strong></h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Não é só a qualidade de produto.</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Deve pensar primeiro como é o seu fio e do quê ele mais precisa para a época.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Mulher é louca para ficar inventado coisa para fazer nos fios e isso atinge totalmente nos procedimentos de cuidados com o&nbsp;fio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"46c3\">Vale lembrar:&nbsp;🤑</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais você inventa na sua juba, mais custos terá com produtos de lavagem e hidratação.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Não tem chochorô!!!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":13} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ-1.png\" alt=\"\" class=\"wp-image-13\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"47d9\">💧 Para você&nbsp;<strong>investir&nbsp;melhor…</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"3ecc\">Precisa primeiro se&nbsp;<strong>AUTO- AVALIAR</strong>&nbsp;para que o shampoo tenha um resultado realmente&nbsp;<strong>BOM</strong>.</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><em>Ele só pode ser bom se a sua avaliação estiver afinada com a realidade dos fios.</em></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"a344\">🤔 Antes da compra, pense friamente:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:list -->\n<ul><li><em>Tem química no cabelo? Quantas e quais tipos?</em></li><li><em>O cabelo é grosso ou fino?</em></li><li><em>Crespo, ondulado, liso ou super liso?</em></li><li><em>Que tipo ele é? Oleoso por inteiro, seco, misto… pense porque ele é ou está assim.</em></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Quanto mais químicas e procedimentos estéticos no fio, mais mudará o tipo de cabelo também.</em></strong></p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"8684\">Vou dar alguns exemplos babadeiros focados somente no shampoo, tá?&nbsp;🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><em>Faço um post depois reservado para o condicionamento/ tratamento dos fios.</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"6da5\">1. Resolveu tingir os fios para ser uma nova&nbsp;ruiva!</h3>\n<!-- /wp:heading -->\n\n<!-- wp:image {\"id\":12} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_Jmcm44hTrhIM1HI5n_st5g-1024x623.jpeg\" alt=\"\" class=\"wp-image-12\"/></figure>\n<!-- /wp:image -->', 'Boas opções de shampoo para o seu dia a dia e tipo de cabelo.', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2019-01-17 10:09:44', '2019-01-17 12:09:44', '', 31, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/31-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2019-01-17 10:09:50', '2019-01-17 12:09:50', '<!-- wp:heading {\"level\":3} -->\n<h3 id=\"4ca2\">Na maquiagem traz com a experiência muitos segredinhos. 🌟</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Criamos com a prática aqueles truques de maquiagem super \"pessoais\", digamos assim.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Quando maquiamos as pessoas ou nós mesmas no dia a dia, sacamos da manga aqueles truques e dicas que agilizam a vida. E no final da história, muito se fala na net mas essas pessoais, ninguém sabe.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Novas formas de&nbsp;<strong><em>ver + usar + experimentar&nbsp;</em></strong>durante a prática podem resolver pepinos que antes estavam ali&nbsp;<em>\"empacando a sua vida\".</em></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>ISSO AFETA SEU HÁBITO E SUA CARTEIRA.&nbsp;</strong>🤭</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Ficamos presas nas idéias que vemos nas propagandas ou em conteúdo da internet…</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"id\":8} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/2019/01/1_0qPOXPqrYVpM6tUSYTK7SQ.png\" alt=\"\" class=\"wp-image-8\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4 id=\"8ecd\"><em>DAÍ, AMIGA…&nbsp;</em>🙄</h4>\n<!-- /wp:heading -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p><strong><em>Você compra mais coisa, gasta horrores em coisas \"más ou ménos\" e ainda continua cheia de dúvidas na cabeça.</em></strong></p></blockquote>\n<!-- /wp:quote -->', 'Dicas e truques de maquiagem que ninguém sabe.', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2019-01-17 10:09:50', '2019-01-17 12:09:50', '', 30, 'http://localhost/projetos/amandaschautica_blog/2019/01/17/30-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2019-01-25 11:36:56', '2019-01-25 13:36:56', '<!-- wp:heading -->\n<h2>Anuncie</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Tem interesse em anunciar no blog e crescer junto com a gente?!</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Preencha o formulário e conte sobre sua empresa, serviço ou produto.</p>\n<!-- /wp:paragraph -->', 'Anuncie', '', 'publish', 'closed', 'closed', '', 'anuncie', '', '', '2019-01-25 12:35:10', '2019-01-25 14:35:10', '', 0, 'https://amandakaroline.com.br/amandaschautica/?page_id=49', 0, 'page', '', 0),
(50, 1, '2019-01-25 11:36:56', '2019-01-25 13:36:56', '', 'Anuncie', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2019-01-25 11:36:56', '2019-01-25 13:36:56', '', 49, 'https://amandakaroline.com.br/amandaschautica/49-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2019-01-25 12:35:10', '2019-01-25 14:35:10', '<!-- wp:heading -->\n<h2>Anuncie</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Tem interesse em anunciar no blog e crescer junto com a gente?!</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Preencha o formulário e conte sobre sua empresa, serviço ou produto.</p>\n<!-- /wp:paragraph -->', 'Anuncie', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2019-01-25 12:35:10', '2019-01-25 14:35:10', '', 49, 'https://amandakaroline.com.br/amandaschautica/49-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2019-01-25 12:27:54', '2019-01-25 14:27:54', '<!-- wp:heading -->\n<h2>Anuncie</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Tem interesse em anunciar no blog e crescer junto com a gente?!</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Preencha o formulário e conte sobre sua empresa, serviço ou produto.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"53\" title=\"Formulário de Anúncio\"]\n<!-- /wp:shortcode -->', 'Anuncie', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2019-01-25 12:27:54', '2019-01-25 14:27:54', '', 49, 'https://amandakaroline.com.br/amandaschautica/49-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2019-01-25 11:45:47', '2019-01-25 13:45:47', '<!-- wp:heading -->\n<h2>Anuncie</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><strong>Tem interesse em anunciar no blog e crescer junto com a gente?!</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Preencha o formulário e conte sobre sua empresa, serviço ou produto.</p>\n<!-- /wp:paragraph -->', 'Anuncie', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2019-01-25 11:45:47', '2019-01-25 13:45:47', '', 49, 'https://amandakaroline.com.br/amandaschautica/49-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2019-01-25 12:27:00', '2019-01-25 14:27:00', '[text* nome id:nome placeholder \"Nome\"][email* email id:email placeholder \"E-mail\"][text assunto id:assunto placeholder \"Assunto\"][textarea mensagem id:mensagem placeholder \"mensagem...\"][submit id:enviar \"Enviar\"]\n1\nAmanda Schautica \"[assunto]\"\nAmanda Schautica <info@amandakaroline.com.br>\ndevhcdesenvolvimentos@gmail.com\nDe: [nome] <[email]>\r\nAssunto: [assunto]\r\n\r\nCorpo da mensagem:\r\n[mensagem]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Amanda Schautica (https://amandakaroline.com.br/amandaschautica)\nReply-To: [email]\n\n\n\n\nAmanda Schautica \"[your-subject]\"\nAmanda Schautica <wordpress@amandakaroline.com.br>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Amanda Schautica (https://amandakaroline.com.br/amandaschautica)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Formulário de Anúncio', '', 'publish', 'closed', 'closed', '', 'formulario-de-anuncio', '', '', '2019-01-25 13:52:04', '2019-01-25 15:52:04', '', 0, 'https://amandakaroline.com.br/amandaschautica/?post_type=wpcf7_contact_form&#038;p=53', 0, 'wpcf7_contact_form', '', 0),
(56, 1, '2019-01-25 14:09:43', '2019-01-25 16:09:43', ' ', '', '', 'publish', 'closed', 'closed', '', '56', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=56', 8, 'nav_menu_item', '', 0),
(57, 1, '2019-01-25 14:09:43', '2019-01-25 16:09:43', ' ', '', '', 'publish', 'closed', 'closed', '', '57', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=57', 6, 'nav_menu_item', '', 0),
(58, 1, '2019-01-25 14:09:43', '2019-01-25 16:09:43', ' ', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=58', 7, 'nav_menu_item', '', 0),
(59, 1, '2019-01-25 14:12:59', '2019-01-25 16:12:59', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=59', 2, 'nav_menu_item', '', 0),
(60, 1, '2019-01-25 14:12:59', '2019-01-25 16:12:59', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=60', 3, 'nav_menu_item', '', 0),
(61, 1, '2019-01-25 14:12:59', '2019-01-25 16:12:59', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=61', 4, 'nav_menu_item', '', 0),
(62, 1, '2019-01-25 14:12:59', '2019-01-25 16:12:59', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=62', 5, 'nav_menu_item', '', 0),
(63, 1, '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-01-25 14:12:59', '2019-01-25 16:12:59', '', 0, 'https://amandakaroline.com.br/amandaschautica/?p=63', 1, 'nav_menu_item', '', 0),
(64, 1, '2019-01-25 15:03:41', '2019-01-25 17:03:41', '', 'LOGO PNG', '', 'inherit', 'open', 'closed', '', 'logo-png', '', '', '2019-01-25 15:03:41', '2019-01-25 17:03:41', '', 0, 'https://amandakaroline.com.br/amandaschautica/wp-content/uploads/2019/01/LOGO-PNG.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_termmeta`
--

CREATE TABLE `as_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_terms`
--

CREATE TABLE `as_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_terms`
--

INSERT INTO `as_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Maquiagem', 'maquiagem', 0),
(3, 'Maquiagem', 'maquiagem', 0),
(4, 'Dicas', 'dicas', 0),
(6, 'Destaque', 'destaque', 0),
(7, 'Vídeos', 'videos', 0),
(8, 'Resenha', 'resenha', 0),
(10, 'Outros', 'outros', 0),
(11, 'Menu Principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_term_relationships`
--

CREATE TABLE `as_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_term_relationships`
--

INSERT INTO `as_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(7, 3, 0),
(7, 2, 0),
(11, 4, 0),
(7, 4, 0),
(7, 6, 0),
(11, 6, 0),
(11, 2, 0),
(11, 3, 0),
(30, 2, 0),
(30, 6, 0),
(30, 3, 0),
(30, 4, 0),
(31, 2, 0),
(31, 6, 0),
(31, 3, 0),
(31, 4, 0),
(32, 2, 0),
(32, 6, 0),
(32, 3, 0),
(32, 4, 0),
(33, 2, 0),
(33, 6, 0),
(33, 3, 0),
(33, 4, 0),
(34, 2, 0),
(34, 6, 0),
(34, 3, 0),
(34, 4, 0),
(35, 2, 0),
(35, 6, 0),
(35, 3, 0),
(35, 4, 0),
(36, 2, 0),
(36, 6, 0),
(36, 3, 0),
(36, 4, 0),
(37, 2, 0),
(37, 6, 0),
(37, 3, 0),
(37, 4, 0),
(38, 2, 0),
(38, 6, 0),
(38, 3, 0),
(38, 4, 0),
(38, 10, 0),
(63, 11, 0),
(38, 8, 0),
(38, 7, 0),
(56, 11, 0),
(57, 11, 0),
(58, 11, 0),
(59, 11, 0),
(60, 11, 0),
(61, 11, 0),
(62, 11, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_term_taxonomy`
--

CREATE TABLE `as_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_term_taxonomy`
--

INSERT INTO `as_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'category', '', 0, 11),
(3, 3, 'post_tag', '', 0, 11),
(4, 4, 'post_tag', '', 0, 11),
(6, 6, 'category', '', 0, 11),
(7, 7, 'category', '', 0, 1),
(8, 8, 'category', '', 0, 1),
(10, 10, 'category', '', 0, 1),
(11, 11, 'nav_menu', '', 0, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_usermeta`
--

CREATE TABLE `as_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_usermeta`
--

INSERT INTO `as_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'amandaschautica'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'as_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'as_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"af25da82de411c1b36476173a7827be0346570aca71b24fa8cacdb3ba938e82d\";a:4:{s:10:\"expiration\";i:1548596186;s:2:\"ip\";s:15:\"187.255.129.233\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1548423386;}}'),
(17, 1, 'as_dashboard_quick_press_last_post_id', '48'),
(18, 1, 'as_user-settings', 'libraryContent=browse'),
(19, 1, 'as_user-settings-time', '1547724292'),
(20, 1, 'wysija_pref', 'YTowOnt9'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:26:\"add-post-type-linhadotempo\";i:1;s:12:\"add-post_tag\";}'),
(24, 1, 'nav_menu_recently_edited', '11'),
(25, 1, 'as_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1548522176;}'),
(21, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"187.255.129.0\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_users`
--

CREATE TABLE `as_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `as_users`
--

INSERT INTO `as_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'amandaschautica', '$P$Bf6pZNP0UDZzdun2kv5F0nOLRuRsic0', 'amandaschautica', 'devhcdesenvolvimentos@gmail.com', '', '2019-01-16 13:14:07', '', 0, 'amandaschautica');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_campaign`
--

CREATE TABLE `as_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_campaign`
--

INSERT INTO `as_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', ''),
(2, 'Newsletter - Amanda Shcautica', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_campaign_list`
--

CREATE TABLE `as_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_campaign_list`
--

INSERT INTO `as_wysija_campaign_list` (`list_id`, `campaign_id`, `filter`) VALUES
(1, 2, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_custom_field`
--

CREATE TABLE `as_wysija_custom_field` (
  `id` mediumint(9) NOT NULL,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_email`
--

CREATE TABLE `as_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_email`
--

INSERT INTO `as_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Problemas de visualização? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"http://localhost/projetos/amandaschautica_blog/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"http://localhost/projetos/amandaschautica_blog/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/amandaschautica_blog/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 4:</strong> e o rodapé?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"http://localhost/projetos/amandaschautica_blog/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1547655123, 1547655123, NULL, 'info@localhost', 'amandaschautica', 'info@localhost', 'amandaschautica', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyOToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjk6InRodW1iX3VybCI7czoxMzc6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLTE1MHgxNTAucG5nIjt9fX0=', 'YTo0OntzOjc6InZlcnNpb24iO3M6NjoiMi4xMC4yIjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMjk6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvaGVhZGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NzI7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImhlYWRlciI7fXM6NDoiYm9keSI7YTo5OntzOjc6ImJsb2NrLTEiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTYwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNVG84TDNOMGNtOXVaejRnWldrc0lHTnNhWEYxWlNCdVpYTjBaU0IwWlhoMGJ5RThMMmd5UGp4d1BsQmhjbUVnWldScGRHRnlMQ0J6YVcxd2JHVnpiV1Z1ZEdVZ1kyeHBjWFZsSUc1bGMzUmxJR0pzYjJOdklHUmxJSFJsZUhSdkxqd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjkxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTMiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6ODA6IlBHZ3lQanh6ZEhKdmJtYytVR0Z6YzI4Z01qbzhMM04wY205dVp6NGdZbkpwYm5GMVpTQmpiMjBnWlhOMFlTQnBiV0ZuWlcwOEwyZ3lQZz09Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNCI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czo3NjoiUEhBK1VHOXphV05wYjI1bElHOGdjMlYxSUcxdmRYTmxJR0ZqYVcxaElHUmhJR2x0WVdkbGJTRERvQ0JsYzNGMVpYSmtZUzQ4TDNBKyI7fXM6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTI5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6NToid2lkdGgiO2k6MjgxO3M6NjoiaGVpZ2h0IjtpOjE5MDtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NDtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNSI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjU7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjkxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTYiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MzAwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNem84TDNOMGNtOXVaejRnYzI5c2RHVWdZMjl1ZEdYRHVtUnZJR0Z4ZFdrOEwyZ3lQanh3UGtGeWNtRnpkR1VnWlNCemIyeDBaU0E4YzNSeWIyNW5QblJsZUhSdkxDQndiM04wY3l3Z1pHbDJhWE52Y21Wekxqd3ZjM1J5YjI1blBpQldaV3BoSUc1dklHeGhaRzhnWkdseVpXbDBieUU4TDNBK1BIQStWbTlqdzZvZ2NHOWtaU0JoZE1PcElHTnlhV0Z5SUR4emRISnZibWMrWTI5dGNHRnlkR2xzYUdGdFpXNTBiM01nYzI5amFXRnBjend2YzNSeWIyNW5QaUJqYjIxdklHVnpkR1Z6T2p3dmNEND0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwNToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZmFjZWJvb2sucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjE7YTo3OntzOjM6InVybCI7czozMjoiaHR0cDovL3d3dy50d2l0dGVyLmNvbS9tYWlsX3BvZXQiO3M6MzoiYWx0IjtzOjc6IlR3aXR0ZXIiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6MTA0OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi90d2l0dGVyLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9aToyO2E6Nzp7czozOiJ1cmwiO3M6MzM6Imh0dHBzOi8vcGx1cy5nb29nbGUuY29tLytNYWlscG9ldCI7czozOiJhbHQiO3M6NjoiR29vZ2xlIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwMzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjkxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTkiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTg4OiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdORG84TDNOMGNtOXVaejRnWlNCdklISnZaR0Z3dzZrL1BDOW9NajQ4Y0Q1TmIyUnBabWx4ZFdVZ2J5QmpiMjUwWmNPNlpHOGdaRzhnY205a1lYRERxU0J1WVNBOGMzUnliMjVuUG5ERG9XZHBibUVnWkdVZ2IzRERwOE8xWlhNOEwzTjBjbTl1Wno0Z1pHOGdUV0ZwYkZCdlpYUXVQQzl3UGc9PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjk7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO319czo2OiJmb290ZXIiO2E6NTp7czo0OiJ0ZXh0IjtOO3M6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTI5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2Zvb3Rlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjQ2O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJmb290ZXIiO319', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em Amanda Schautica', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1547655123, 1547655123, NULL, 'info@localhost', 'amandaschautica', 'info@localhost', 'amandaschautica', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL),
(3, 2, 'Newsletter - Amanda Shcautica', NULL, 1547655153, 1547655153, NULL, 'info@localhost', 'amandaschautica', 'info@localhost', 'amandaschautica', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToyOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyOToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjk6InRodW1iX3VybCI7czoxMzc6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLTE1MHgxNTAucG5nIjt9fXM6NzoiZGl2aWRlciI7YTozOntzOjM6InNyYyI7czo5MToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9fQ==', 'YTo0OntzOjc6InZlcnNpb24iO3M6NjoiMi4xMC4yIjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMDQ6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvdHJhbnNwYXJlbnQucG5nIjtzOjU6IndpZHRoIjtpOjYwMDtzOjY6ImhlaWdodCI7aTo4NjtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjE7fXM6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MTtzOjQ6InR5cGUiO3M6NjoiaGVhZGVyIjt9czo0OiJib2R5IjthOjk6e3M6NzoiYmxvY2stMSI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czoxMTk6IjxoMj48c3Ryb25nPlBhc3NvIDE6PC9zdHJvbmc+IGVpLCBjbGlxdWUgbmVzdGUgdGV4dG8hPC9oMj48cD5QYXJhIGVkaXRhciwgc2ltcGxlc21lbnRlIGNsaXF1ZSBuZXN0ZSBibG9jbyBkZSB0ZXh0by48L3A+Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjkxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTMiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6NTg6IjxoMj48c3Ryb25nPlBhc3NvIDI6PC9zdHJvbmc+IGJyaW5xdWUgY29tIGVzdGEgaW1hZ2VtPC9oMj4iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTozO3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay00IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjU3OiI8cD5Qb3NpY2lvbmUgbyBzZXUgbW91c2UgYWNpbWEgZGEgaW1hZ2VtIMOgIGVzcXVlcmRhLjwvcD4iO31zOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyOToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjU6IndpZHRoIjtpOjI4MTtzOjY6ImhlaWdodCI7aToxOTA7czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjQ7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTUiO2E6NTp7czo4OiJwb3NpdGlvbiI7aTo1O3M6NDoidHlwZSI7czo3OiJkaXZpZGVyIjtzOjM6InNyYyI7czo5MToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay02IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjIyNDoiPGgyPjxzdHJvbmc+UGFzc28gMzo8L3N0cm9uZz4gc29sdGUgY29udGXDumRvIGFxdWk8L2gyPjxwPkFycmFzdGUgZSBzb2x0ZSA8c3Ryb25nPnRleHRvLCBwb3N0cywgZGl2aXNvcmVzLjwvc3Ryb25nPiBWZWphIG5vIGxhZG8gZGlyZWl0byE8L3A+PHA+Vm9jw6ogcG9kZSBhdMOpIGNyaWFyIDxzdHJvbmc+Y29tcGFydGlsaGFtZW50b3Mgc29jaWFpczwvc3Ryb25nPiBjb21vIGVzdGVzOjwvcD4iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwNToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZmFjZWJvb2sucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjE7YTo3OntzOjM6InVybCI7czozMjoiaHR0cDovL3d3dy50d2l0dGVyLmNvbS9tYWlsX3BvZXQiO3M6MzoiYWx0IjtzOjc6IlR3aXR0ZXIiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6MTA0OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi90d2l0dGVyLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9aToyO2E6Nzp7czozOiJ1cmwiO3M6MzM6Imh0dHBzOi8vcGx1cy5nb29nbGUuY29tLytNYWlscG9ldCI7czozOiJhbHQiO3M6NjoiR29vZ2xlIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwMzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9hbWFuZGFzY2hhdXRpY2FfYmxvZy93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjkxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2FtYW5kYXNjaGF1dGljYV9ibG9nL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTkiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTM5OiI8aDI+PHN0cm9uZz5QYXNzbyA0Ojwvc3Ryb25nPiBlIG8gcm9kYXDDqT88L2gyPjxwPk1vZGlmaXF1ZSBvIGNvbnRlw7pkbyBkbyByb2RhcMOpIG5hIDxzdHJvbmc+cMOhZ2luYSBkZSBvcMOnw7Vlczwvc3Ryb25nPiBkbyBNYWlsUG9ldC48L3A+Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6OTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fX1zOjY6ImZvb3RlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMDQ6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvYW1hbmRhc2NoYXV0aWNhX2Jsb2cvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvdHJhbnNwYXJlbnQucG5nIjtzOjU6IndpZHRoIjtpOjYwMDtzOjY6ImhlaWdodCI7aTo4NjtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjE7fXM6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MTtzOjQ6InR5cGUiO3M6NjoiZm9vdGVyIjt9fQ==', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiRkZGRkZGIjt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJGRkZGRkYiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTM7czoxMDoiYmFja2dyb3VuZCI7czo2OiJGRkZGRkYiO31zOjY6ImZvb3RlciI7YTo0OntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMztzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImNjY2NjYyI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6Mjg7fXM6MjoiaDIiO2E6Mzp7czo1OiJjb2xvciI7czo2OiI0MjQyNDIiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MjY7fXM6MjoiaDMiO2E6Mzp7czo1OiJjb2xvciI7czo2OiI0MjQyNDIiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMTt9fQ==');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_email_user_stat`
--

CREATE TABLE `as_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_email_user_url`
--

CREATE TABLE `as_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_form`
--

CREATE TABLE `as_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_form`
--

INSERT INTO `as_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIxIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo3MzoiVmVqYSBzdWEgY2FpeGEgZGUgZW50cmFkYSBvdSBwYXN0YSBkZSBzcGFtIHBhcmEgY29uZmlybWFyIHN1YSBhc3NpbmF0dXJhLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mzp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjM6e3M6NToibGFiZWwiO3M6NDoiTm9tZSI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjEiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6OToiZmlyc3RuYW1lIjtzOjQ6Im5hbWUiO3M6NDoiTm9tZSI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjY6InBhcmFtcyI7YTozOntzOjU6ImxhYmVsIjtzOjU6IkVtYWlsIjtzOjEyOiJsYWJlbF93aXRoaW4iO3M6MToiMSI7czo4OiJyZXF1aXJlZCI7czoxOiIxIjt9czo4OiJwb3NpdGlvbiI7aToyO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo0OiJuYW1lIjtzOjU6IkVtYWlsIjt9czo3OiJibG9jay0zIjthOjU6e3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6MDoiIjt9czo4OiJwb3NpdGlvbiI7aTozO3M6NDoidHlwZSI7czo2OiJzdWJtaXQiO3M6NToiZmllbGQiO3M6Njoic3VibWl0IjtzOjQ6Im5hbWUiO3M6NjoiRW52aWFyIjt9fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_list`
--

CREATE TABLE `as_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_list`
--

INSERT INTO `as_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Minha primeira lista', 'minha-primeira-lista', 'A lista criada automaticamente na instalação do MailPoet.', 0, 0, 1, 1, 1547655122, 0),
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1547655123, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_queue`
--

CREATE TABLE `as_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_subscriber_ips`
--

CREATE TABLE `as_wysija_subscriber_ips` (
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_url`
--

CREATE TABLE `as_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_url_mail`
--

CREATE TABLE `as_wysija_url_mail` (
  `email_id` int(11) NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_user`
--

CREATE TABLE `as_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_user`
--

INSERT INTO `as_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'devhcdesenvolvimentos@gmail.com', '', '', '', '0', NULL, NULL, NULL, '09ece64d8d6ed15afb3b1bc14ab9a200', 1547655124, 1, 'gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_user_field`
--

CREATE TABLE `as_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_user_field`
--

INSERT INTO `as_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_user_history`
--

CREATE TABLE `as_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `as_wysija_user_list`
--

CREATE TABLE `as_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `as_wysija_user_list`
--

INSERT INTO `as_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1547655122, 0),
(2, 1, 1547655123, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `as_commentmeta`
--
ALTER TABLE `as_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `as_comments`
--
ALTER TABLE `as_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `as_links`
--
ALTER TABLE `as_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `as_options`
--
ALTER TABLE `as_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `as_popularpostsdata`
--
ALTER TABLE `as_popularpostsdata`
  ADD PRIMARY KEY (`postid`);

--
-- Indexes for table `as_popularpostssummary`
--
ALTER TABLE `as_popularpostssummary`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `postid` (`postid`),
  ADD KEY `view_date` (`view_date`),
  ADD KEY `view_datetime` (`view_datetime`);

--
-- Indexes for table `as_postmeta`
--
ALTER TABLE `as_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `as_posts`
--
ALTER TABLE `as_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `as_termmeta`
--
ALTER TABLE `as_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `as_terms`
--
ALTER TABLE `as_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `as_term_relationships`
--
ALTER TABLE `as_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `as_term_taxonomy`
--
ALTER TABLE `as_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `as_usermeta`
--
ALTER TABLE `as_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `as_users`
--
ALTER TABLE `as_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `as_wysija_campaign`
--
ALTER TABLE `as_wysija_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `as_wysija_campaign_list`
--
ALTER TABLE `as_wysija_campaign_list`
  ADD PRIMARY KEY (`list_id`,`campaign_id`);

--
-- Indexes for table `as_wysija_custom_field`
--
ALTER TABLE `as_wysija_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `as_wysija_email`
--
ALTER TABLE `as_wysija_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `as_wysija_email_user_stat`
--
ALTER TABLE `as_wysija_email_user_stat`
  ADD PRIMARY KEY (`user_id`,`email_id`);

--
-- Indexes for table `as_wysija_email_user_url`
--
ALTER TABLE `as_wysija_email_user_url`
  ADD PRIMARY KEY (`user_id`,`email_id`,`url_id`);

--
-- Indexes for table `as_wysija_form`
--
ALTER TABLE `as_wysija_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `as_wysija_list`
--
ALTER TABLE `as_wysija_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `as_wysija_queue`
--
ALTER TABLE `as_wysija_queue`
  ADD PRIMARY KEY (`user_id`,`email_id`),
  ADD KEY `SENT_AT_INDEX` (`send_at`);

--
-- Indexes for table `as_wysija_subscriber_ips`
--
ALTER TABLE `as_wysija_subscriber_ips`
  ADD PRIMARY KEY (`created_at`,`ip`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `as_wysija_url`
--
ALTER TABLE `as_wysija_url`
  ADD PRIMARY KEY (`url_id`);

--
-- Indexes for table `as_wysija_url_mail`
--
ALTER TABLE `as_wysija_url_mail`
  ADD PRIMARY KEY (`email_id`,`url_id`);

--
-- Indexes for table `as_wysija_user`
--
ALTER TABLE `as_wysija_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- Indexes for table `as_wysija_user_field`
--
ALTER TABLE `as_wysija_user_field`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `as_wysija_user_history`
--
ALTER TABLE `as_wysija_user_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `as_wysija_user_list`
--
ALTER TABLE `as_wysija_user_list`
  ADD PRIMARY KEY (`list_id`,`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `as_commentmeta`
--
ALTER TABLE `as_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_comments`
--
ALTER TABLE `as_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_links`
--
ALTER TABLE `as_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_options`
--
ALTER TABLE `as_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;

--
-- AUTO_INCREMENT for table `as_popularpostssummary`
--
ALTER TABLE `as_popularpostssummary`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `as_postmeta`
--
ALTER TABLE `as_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `as_posts`
--
ALTER TABLE `as_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `as_termmeta`
--
ALTER TABLE `as_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_terms`
--
ALTER TABLE `as_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `as_term_taxonomy`
--
ALTER TABLE `as_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `as_usermeta`
--
ALTER TABLE `as_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `as_users`
--
ALTER TABLE `as_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_wysija_campaign`
--
ALTER TABLE `as_wysija_campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `as_wysija_custom_field`
--
ALTER TABLE `as_wysija_custom_field`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_wysija_email`
--
ALTER TABLE `as_wysija_email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `as_wysija_form`
--
ALTER TABLE `as_wysija_form`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_wysija_list`
--
ALTER TABLE `as_wysija_list`
  MODIFY `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `as_wysija_url`
--
ALTER TABLE `as_wysija_url`
  MODIFY `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_wysija_url_mail`
--
ALTER TABLE `as_wysija_url_mail`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_wysija_user`
--
ALTER TABLE `as_wysija_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_wysija_user_field`
--
ALTER TABLE `as_wysija_user_field`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `as_wysija_user_history`
--
ALTER TABLE `as_wysija_user_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
