<?php 
/**
*	Template Name: Anuncie
*	Template Description: Template para a página anuncie
*
*	@package amandaschautica
*/
get_header();
?>
	<div class="pg pg-anuncie">
		<div class="containerLagura">
			<div class="conteudoPagina">
				<h2>Anuncie</h2>
				<p><strong>Tem interesse em anunciar no blog e crescer junto com a gente?!</strong></p>
				<p>Preencha o formulário e conte sobre sua empresa, serviço ou produto.</p>
			</div>
			<div class="formularioContato">
				<?php echo do_shortcode('[contact-form-7 id="53" title="Formulário de Anúncio"]'); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>