<?php
/**
 * Template Name: Quem Somos
 * Description: Quem Somos
 *
 * @package amandaschautica
 */
	$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoBlog = $fotoBlog[0];
get_header(); ?>
<!-- PAGINA QUEM SOMOS -->
<div class="pg pg-quemSomos">
	<!-- BANNER PAGINA QUEM SOMOS -->
	<figure class="bannerPost" style="background:url(<?php echo $fotoBlog ?>)">
		<h1><?php echo get_the_title();  ?></h1>
	</figure>

	<!-- CONTAINER PARA CENTRALIZAR O TEXTO -->
	<div class="container">
		<!-- ARTICLE PARA DEFINIR A ESTRUTURA DO CONTEUDO -->
		<article  class="textoQuemSomos">
			<?php while ( have_posts() ):the_post(); echo the_content();  endwhile; ?>
		</article>
	</div>

	<!-- LINHA DO TEMPO -->
	<section class="linhadoTempo hidden" style="display: none;">
		<h6 class="hidden">Linha do tempo</h6>
		<hr>

		<div class="carrossellLinhadoTempo" class="owl-Carousel" id="carrossellLinhadoTempo">
			<?php 
				$i = 0;
				//LOOP DE POST DESTAQUES
				$postLinhadotempo = new WP_Query( array( 'post_type' => 'linhadotempo', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $postLinhadotempo->have_posts() ) : $postLinhadotempo->the_post();
				$fotolinhadoTempo = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotolinhadoTempo = $fotolinhadoTempo[0];
				$data_linha = rwmb_meta('MinhaCasaSolar_data_linha');

				if ($i == 0):
			 ?>
			<div class="item ativo primeiro" data-descricao="<?php echo get_the_content() ?>">
				<div class="areaconteudo">
					<figure>
						<img src="<?php echo $fotolinhadoTempo ?>" alt="<?php echo get_the_title() ?>">
					</figure>
					<span><?php echo $data_linha ?></span>
				</div>
			</div>
			<?php else: ?>

			<div class="item" data-descricao="<?php echo get_the_content() ?>">
				<div class="areaconteudo">
					<figure>
						<img src="<?php echo $fotolinhadoTempo ?>" alt="<?php echo get_the_title() ?>">
					</figure>
					<span><?php echo $data_linha ?></span>
				</div>
			</div>

			<?php endif; ?>
		<?php  $i++;endwhile; wp_reset_query(); ?>
			
		</div>
		
		<div class="textoLinhadoTempo">
			<p></p>
			<button id="btnMudarCarrossel"></button>
			<button id="btnMudarCarrosselright"></button>
			
		</div>
	
	</section>
</div>
<?php get_footer(); ?>