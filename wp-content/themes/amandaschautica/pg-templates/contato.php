<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package amandaschautica
 */
	$fotoContato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoContato = $fotoContato[0];
get_header(); ?>
<!-- PAGINA DE CONTATO-->
<div class="pg pg-contato">
	
	<!-- BANNER PAGINA CONTATOS -->
	<figure class="bannerPost" style="background: url(<?php echo $fotoContato ?>)">
	</figure>

	<div class="containerLagura">
		<div class="row">
			
			<div class="col-sm-6">
				<!-- ARTICLE PARA DEFINIAR A ESTRUTURA DO TEXTO -->
				<article>
					<?php while ( have_posts() ):the_post(); echo the_content();  endwhile; ?>
				</article>
				
				<!-- DIV QUE REPRESENTA A AREA DE CONTATO -->
				<div class="areaContato">
					<div class="col-sm-6">
						<!-- DIV ENDEREÇO -->
						<div class="endereco">
							<span>Endereço</span>
							<a href="#" ><?php echo $configuracao['opt_endereco'] ?></a>
						</div>
					</div>

					<div class="col-sm-6">
						<!-- DIV CONTATO -->
						<div class="contato">	
							<span>Contatos</span>
							<?php 
								foreach ($configuracao['opt_contatos'] as $configuracao['opt_contatos']):
									$configuracao['opt_contatos'] = $configuracao['opt_contatos'];
									$opt_contatos = explode(",", $configuracao['opt_contatos']); 
							?>
							<a href="tel:<?php echo $opt_contatos[1] ?>" target="_blank" ><?php echo $opt_contatos[0] ?> <?php echo $opt_contatos[1] ?></a>
						<?php endforeach?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="formContato">
					<!-- NEWSLLATER -->
					<div class="areaNewsllater">
						<figure>
							<img src="<?php echo get_bloginfo("template_url"); ?>/img/if_misc-_message_1276855.png" alt="">
						</figure>
						
						<p>Envie uma mensagem</p>
						<div class="form">
							<?php echo do_shortcode('[contact-form-7 id="27" title="Formulário de contato"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>			

	</div>
	<!-- MAPA -->	
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.460862804578!2d-51.47599483850059!3d-25.389379530462108!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef37cb6ea7d763%3A0xdf09c70f3bce1b10!2sSaldanha+Marinho!5e0!3m2!1spt-BR!2sbr!4v1547724473376" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php get_footer(); ?>