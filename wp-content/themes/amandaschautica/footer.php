<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amandaschautica
 */
global $configuracao;
?>
 
	<!-- BOTÃO VOLTAR AO TOPO -->
	<div class="backToTop" style="display: none">
	    <span id="subir">
	        <i class="fas fa-chevron-up"></i>
	    </span>
	</div>

<div class="instagramRodape">
	<div id="instafeed">
	</div>
</div>

<footer class="rodape_as">
	<div class="containerLagura">
		<div class="categoriasRodape">
			<div class="row">
				<div class="col-md-6">
					<p>Categorias</p>
				</div>
				<div class="col-md-6">
					<ul class="listaCategorias">
						<?php
							$categorias = get_categories(); 
							foreach ($categorias as $categorias):
								if($categorias->name != 'Destaque'):
						?>
						<li class="categoria">
							<a href="<?php echo get_category_link($categorias->cat_ID); ?>"><?php echo $categorias->name; ?></li></a>
						<?php endif; endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="menu_rodape_as">
			<div class="row">
				<div class="col-md-9">
					<div class="menuRodape">
						<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => '',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="redesSociaisRodape">
						<ul>
							<li>
								<a href="<?php echo $configuracao['opt_facebook'] ?>">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li>
								<a href="<?php echo $configuracao['opt_instagram'] ?>">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="<?php echo $configuracao['opt_youtube'] ?>">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="logoRodape">
			<img src="<?php echo get_template_directory_uri() ?>/img/logoBranca.png" alt="<?php echo get_bloginfo(); ?>">
		</div>
	</div>
</footer>
<div class="copyrightRodape">
	<p><i class="far fa-copyright"></i> 2019 - Todos os direitos reservados - Desenvolvido por <a href="https://hcdesenvolvimentos.com.br"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="HC Desenvolvimentos"></a></p>
</div>

<script src="https://matthewelsom.com/assets/js/libs/instafeed.min.js"></script>
<script>
	var userFeed = new Instafeed({
		get: 'user',
		userId: '1475144403',
		clientId: '714aa9a8bacd4803afc79eea969fbe0d',
		accessToken: '1475144403.714aa9a.b0e7f1545afd48d4869ee264c96e9ea1',
		resolution: 'standard_resolution',
		template: '<a href="{{link}}" class="item" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes"><i class="fas fa-heart"></i> {{likes}}</span><span class="comments"> <i class="fas fa-comment"></i> {{comments}}</span></small></div></a>',
		sortBy: 'most-recent',
		limit: 6,
		links: false,
		target: 'instafeed'
	});

	userFeed.run();
</script>
<?php wp_footer(); ?>

</body>
</html>
