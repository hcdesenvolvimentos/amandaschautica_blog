<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package amandaschautica
 */

$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoBlog = $fotoBlog[0];

global $post;
$categories = get_the_category();
$idPostAtual = $post->ID;

get_header();
?>
<!-- PAGINA DE POST	-->
<div class="pg pg-post">
	
	<!-- BANNER DO POST	-->
	<figure class="bannerPost" style="background: url(<?php echo $fotoBlog  ?>)">
	</figure>

	<!-- DEFININDO CONTAINER -->
	<div class="containerLagura">

		<!-- DEFININDO COLUNAS -->
		<div class="row">

			<!-- DEFININDO A COLUNA E O TAMANHO DA COLUNA -->
			<div class="col-sm-8">
				
				<!-- ARTICLE PARA DEFINIR A ESTRUTURA DO POST -->
				<article class="estruturaPost">

					<!-- CATEGORIA DO POST -->
					<?php 
						foreach ($categories as $categories){
							if ($categories->name != "Destaque"){
								$nomeCategoria = $categories->name;
								$idCategoria = $categories->cat_ID;
							}
						} 
					?>
					<h2><?php echo $nomeCategoria ?></h2>

					<!-- TITULO DO POST -->
					<h1><?php echo get_the_title() ?></h1>

					<!--DATA DO POST -->
					<strong><?php echo  get_the_date('j F, Y'); ?></strong>

					<!-- TEXTO DO POST -->
					<div class="textoDoPost">

						<?php 
						if ( have_posts() ) : while( have_posts() ) : the_post();
							
							echo the_content();

						endwhile;endif; wp_reset_query(); ?>
					</div>

				</article>

				<div class="disqus">
					<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>
				</div>
				
				<!-- LISTA DE POSTS RELACIONADOS -->
				<div class="postsRelacionados">
					<span>Você pode gostar também de...</span>

					<?php 
				        //FILTRO CATEGORIA POST            
				        $filtroDePostPorCategoria = new WP_Query(array(
				            // TIPO DE POST
				            'post_type'     => 'post',
				            
				            // POST POR PAGINA
				            'posts_per_page'   => 3,
				            // FILTRANDO PELA CATEGORIA DESTAQUE
				            'tax_query'     => array(
				                array(
				                    // TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
				                    'taxonomy' => 'category',
				                    // PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
				                    'field'    => 'term_id',
				                    // SLUG DA CATEGORIA
				                    'terms'    => $idCategoria,
				                    )
				                )
				            )
				        );    
				    
				    ?>
					<ul>
						<?php 
			        	// LOOP DE POST
							while ( $filtroDePostPorCategoria->have_posts() ) : $filtroDePostPorCategoria->the_post();
								$fotoDestaquePostCategoria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDestaquePostCategoria = $fotoDestaquePostCategoria[0];
									global $post;
			                        $idpostLoop = $post;
			                        if ($idPostAtual != $idpostLoop->ID):
					 	?>	
					
						<li>
							<a href="<?php echo get_permalink(); ?>" class="linkPost">
							<article class="estruturaPostRelacionado">
								<figure class="imagemDestaquePost" style="background: url(<?php echo $fotoDestaquePostCategoria	?>)">
								</figure>
								<h2><?php echo $nomeCategoria; ?></h2>
								<h1><?php echo get_the_title(); ?></h1>
								<span><?php the_time('j F, Y');?></span>
								<p><?php customExcerpt(120); ?></p>
							</article>
							</a>	
							<?php disqus_count('myshortcode'); ?>
						</li>

					<?php endif; endwhile; ?>
					</ul>
				</div>
				
			</div>

			<!-- DEFININDO A COLUNA E O TAMANHO DA COLUNA -->
			<div class="col-sm-4">
				<!-- SIDEBAR-->
				<?php include (TEMPLATEPATH . "/sidebar-single.php");  ?>
			</div>

		</div>


	</div>
	
</div>
<?php
get_footer();