<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amandaschautica
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
global $configuracao;
?>

<!-- OUTROS POSTS -->
<aside>
	
	<!-- LISTA DE POSTS -->
	<ul style="display: none;">
		<?php 

			// LOOP DE POSTS
			$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'rand', 'posts_per_page' => -3 ) );
		    while ( $posts->have_posts() ) : $posts->the_post();
				
				global $post;
				$categories = get_the_category();

				// FOTO DESTACADA
				$fotoDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaquePost = $fotoDestaquePost[0];

		?>	
		<!-- POST -->
		<li>
			<!-- LINK DO POST -->
			<a href="<?php echo get_permalink() ?> " class="linkPost">
				<div class="row">
					<!-- IMAGEM DE POST -->
					<div class="col-sm-4">
						<figure class="imagemPost" style="background:url(<?php echo $fotoDestaquePost ?>)"></figure>
					</div>
					<!-- DESCRÇÃO DO POST -->
					<div class="col-sm-8">
						<div class="texto">
							<!-- CATEGORIA DO POST -->
							<?php 
								foreach ($categories as $categories){
									if ($categories->name != "Destaque"){
										$nomeCategoria = $categories->name;
									}
								} 
							?>
							<!-- CETEGORIA -->
							<h3><?php echo $nomeCategoria ?></h3>
							<!-- TÍTULO DO POST -->
							<h2><?php echo get_the_title() ?></h2>
							<!-- DATA DO POST -->
							<strong><?php echo  get_the_date('j F, Y'); ?></strong>
							
						</div>
					</div>
				</div>
			</a>

		</li>
		<?php endwhile; wp_reset_query(); ?>
	</ul>

	<div class="biografiaSidebar">
		<p class="nome">Amanda Schautica</p>
		<div class="imagemPessoal">
			<img src="<?php echo $configuracao['fotoBio']['url'] ?>" alt="Amanda Schautica">
		</div>
		<div class="descricaoBio">
			<p>
				<?php echo $configuracao['descricaoBio'] ?>
			</p>
		</div>
		<div class="redesSociaisBio">
			<ul>
				<li>
					<a href="<?php echo $configuracao['opt_facebook'] ?>">
						<i class="fab fa-facebook-f"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo $configuracao['opt_instagram'] ?>">
						<i class="fab fa-instagram"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	
	<!-- NEWSLLATER -->
	<!-- <div class="areaNewsllater">
		<figure>
			<img src="<?php //echo get_bloginfo("template_url"); ?>/img/if_a_9_2578125.png" alt="">
		</figure>
		
		<p>Receba conteúdo em primeira mão</p>
		<div class="form"> -->
			
			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<!-- <script type="text/javascript" src="https://amandakaroline.com.br/amandaschautica/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
			<script type="text/javascript" src="https://amandakaroline.com.br/amandaschautica/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
			<script type="text/javascript" src="https://amandakaroline.com.br/amandaschautica/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
			<script type="text/javascript" src="https://amandakaroline.com.br/amandaschautica/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
			<script type="text/javascript">
				/* <![CDATA[ */
				var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"https://amandakaroline.com.br/amandaschautica/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
				/* ]]> */
			</script><script type="text/javascript" src="https://amandakaroline.com.br/amandaschautica/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
 -->			<!--END Scripts-->

			<!-- <div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5c3f581abe293-1" class="wysija-msg ajax"></div><form id="form-wysija-html5c3f581abe293-1" method="post" action="#wysija" class="widget_wysija html_wysija">
					<input type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="Nome" placeholder="Nome" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
					</span>
					<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
					</span>
				<input class="wysija-submit wysija-submit-field" type="submit" value="" />

				<input type="hidden" name="form_id" value="1" />
				<input type="hidden" name="action" value="save" />
				<input type="hidden" name="controller" value="subscribers" />
				<input type="hidden" value="1" name="wysija-page" />

				<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
			</form>
		</div>
		</div> -->
	<!-- </div> -->

	<!-- PARCEIROS -->
	<div class="parcerias">
		<?php  ?>
		<h3>Parceiros</h3>
		<ul class="listaParcerias owl-Carousel" id="carrosselParceiros">

			<?php
			$parceiros = new WP_Query( array('post_type' => 'parceiro', 'orderby' => 'id', 'posts_per_page' => '-1') );

			while ($parceiros->have_posts()): $parceiros->the_post();
				$fotoParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoParceiro = $fotoParceiro[0];

			?>

			<li class="item"><img src="<?php echo $fotoParceiro; ?>" alt=""></li>

			<?php endwhile; wp_reset_query(); ?>
		</ul>
	</div>

	<!-- TAGS -->
	<div class="areaTags">
		<h3>Tags</h3>
		<div class="tags">
			<?php
				$post_tags = get_the_tags();  
				if ( $post_tags ):

					foreach( $post_tags as $tag ):
			?>
			<a href="<?php bloginfo('url');?>/tag/<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
			<?php
				  	endforeach;
				endif;
			?>
		</div>
	</div>
	
	<!-- BANNERS -->
	<?php if($configuracao['bannerPropaganda']['url']): ?>
	<div class="bannersSidebar">
		<a href="<?php echo $configuracao['link_banner'];  ?>" target="_blank">
			<img src="<?php echo $configuracao['bannerPropaganda']['url'];  ?>" alt="Banner">
		</a>
	</div>
	<?php endif; ?>



	<!-- POSTS MAIS LIDOS -->
	<!-- <div class="postsMaislidos">
		<span>Posts mais lidos</span>

		<?php //dynamic_sidebar( 'sidebar-1' ); ?>
	</div> -->
</aside>