<?php

/**
 * Plugin Name: Base do tema Amanda Schautica
 * Description: Controle base do tema Amanda Schautica
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: https://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseAmandaSchautica () {

		// TIPOS DE CONTEÚDO
		tipoParceiros();

		conteudosAmandaSchautica();

		taxonomiaAmandaSchautica();

		metaboxesAmandaSchautica();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAmandaSchautica (){

		// TIPOS DE CONTEÚDO
		tipolinhaDoTempo();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipolinhaDoTempo() {

		$rotuloslinhaDoTempo= array(
			'name'               => 'Linha do tempo',
			'singular_name'      => 'linha',
			'menu_name'          => 'Linha do tempo',
			'name_admin_bar'     => 'Linha do tempo',
			'add_new'            => 'Adicionar nova',
			'add_new_item'       => 'Adicionar nova linha',
			'new_item'           => 'Nova linha',
			'edit_item'          => 'Editar linha do tempo',
			'view_item'          => 'Ver linha',
			'all_items'          => 'Todas as linhas',
			'search_items'       => 'Buscar linha',
			'parent_item_colon'  => 'Das linhas',
			'not_found'          => 'Nenhuma linha cadastrado.',
			'not_found_in_trash' => 'Nenhuma linha na lixeira.'
		);

		$argslinhaDoTempo 	= array(
			'labels'             => $rotuloslinhaDoTempo,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-megaphone',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'linha-do-tempo' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail', 'editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('linhadotempo', $argslinhaDoTempo);

	}

	function tipoParceiros(){
		$rotulosParceiros = array(
			'name'               => 'Parceiros',
			'singular_name'      => 'parceiro',
			'menu_name'          => 'Parceiros',
			'name_admin_bar'     => 'Parceiros',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo parceiro',
			'new_item'           => 'Novo parceiro',
			'edit_item'          => 'Editar parceiro',
			'view_item'          => 'Ver parceiro',
			'all_items'          => 'Todos parceiros',
			'search_items'       => 'Buscar parceiro',
			'parent_item_colon'  => 'Dos parceiros',
			'not_found'          => 'Nenhum parceiro cadastrado.',
			'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
		);

		$argsParceiros 	= array(
			'labels'             => $rotulosParceiros,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-networking',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'parceiros_' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiro', $argsParceiros);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaAmandaSchautica () {		
		//taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAmandaSchautica(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'AmandaSchautica_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLinhadotempo',
				'title'			=> 'Detalhes da linha',
				'pages' 		=> array( 'linhadotempo' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Data: ',
						'id'    => "{$prefix}data_linha",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);

			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxParceiro',
				'title'			=> 'Detalhes dos parceiros',
				'pages' 		=> array( 'parceiro' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link: ',
						'id'    => "{$prefix}link_parceiro",
						'desc'  => 'Link para o parceiro: ',
						'type'  => 'text',
					),	
					
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAmandaSchautica(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAmandaSchautica(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAmandaSchautica');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAmandaSchautica();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );